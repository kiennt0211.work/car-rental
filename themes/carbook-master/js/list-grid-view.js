$(document).ready(function () {
    $(".btn-grid").click(function () {
        $(".btn-list").removeClass("active");
        $(".btn-grid").addClass("active");
        $(".grid-container").show();
        $(".list-container").hide();
    });

    $(".btn-list").click(function () {
        $(".btn-grid").removeClass("active");
        $(".btn-list").addClass("active");
        $(".grid-container").hide();
        $(".list-container").show();
    });
});