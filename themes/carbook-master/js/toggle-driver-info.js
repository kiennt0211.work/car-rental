const checkbox = document.getElementById('diff');
const driverInfo = document.getElementById('driverInfo');

checkbox.addEventListener('change', function () {
    driverInfo.style.display = checkbox.checked ? 'block' : 'none';
});