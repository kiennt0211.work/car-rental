﻿using CarRental.Core.Models;

namespace CarRental.DataAccess.Repositories.Interfaces
{
    public interface IFeedBackRepository : IBaseRepository<Feedback>
    {
    }
}
