﻿using CarRental.Core.Models;

namespace CarRental.DataAccess.Repositories.Interfaces
{
    public interface IBrandModelRepository : IBaseRepository<BrandModel>
    {
        public Task<List<string?>> GetModelsByBrand(string selectedBrand);

        public Task<List<string?>> GetBrands();
    }
}
