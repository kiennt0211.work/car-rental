﻿using CarRental.Core.Models;

namespace CarRental.DataAccess.Repositories.Interfaces
{
    public interface ICarRepository : IBaseRepository<Car>
    {
        List<Car> GetCarsByUserId(Guid userId);
        List<Car> GetCarsByDate(DateTime pickupDate, DateTime dropOffDate, string? pickupLocation);
        List<Car> GetCarsByCity(string city);
        List<(string City, int CarCount)> GetTopCitiesWithMostCars();
        Task<List<Feedback>> GetFeedbacksByStar(string filter, Account user);
        double GetAverageStarForUser(Account user);

    }
}
