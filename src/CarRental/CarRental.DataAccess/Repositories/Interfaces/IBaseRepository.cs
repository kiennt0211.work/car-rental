﻿using System.Linq.Expressions;

namespace CarRental.DataAccess.Repositories.Interfaces
{
    public interface IBaseRepository<T>
    {
        List<T> GetAll();
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        T? FindByID(object id);
        IEnumerable<T> GetInclude(Expression<Func<T, bool>> filter, params string[] includeProps);
        Task SaveChangeAsync();
        Task<T?> FirstOrDefault(Expression<Func<T, bool>> filter);
    }
}
