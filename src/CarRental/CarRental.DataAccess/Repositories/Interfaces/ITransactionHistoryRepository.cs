﻿using CarRental.Core.Models;

namespace CarRental.DataAccess.Repositories.Interfaces
{
    public interface ITransactionHistoryRepository : IBaseRepository<TransactionHistory>
    {
        List<TransactionHistory> ListTransactionsByUserId(string userId);
    }
}
