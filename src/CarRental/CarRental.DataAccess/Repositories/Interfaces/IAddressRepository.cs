﻿using CarRental.Core.Models;

namespace CarRental.DataAccess.Repositories.Interfaces
{
    public interface IAddressRepository : IBaseRepository<Address>
    {
    }
}
