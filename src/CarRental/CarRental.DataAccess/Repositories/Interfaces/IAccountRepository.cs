﻿using CarRental.Core.Models;

namespace CarRental.DataAccess.Repositories.Interfaces
{
    public interface IAccountRepository : IBaseRepository<Account>
    {
        Account GetAccountByCarId(Guid userId);
    }
}
