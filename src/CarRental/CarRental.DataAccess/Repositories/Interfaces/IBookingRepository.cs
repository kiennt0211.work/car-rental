﻿using CarRental.Core.Models;
using PagedList;

namespace CarRental.DataAccess.Repositories.Interfaces
{
    public interface IBookingRepository : IBaseRepository<Booking>
    {
        List<Booking> GetBookingsForCustomer(Guid userId, int page, int pageSize);

        Task<Booking> CancelBooking(Guid bookingId);
        Task<Booking> ConfirmPickup(Guid bookingId);
        Car GetCarByBookingId(Guid bookingId);
        Task<Booking> RateCar(Guid bookingId, int ratings, string feedbackContent);
        int CountBooking(Guid carId);
    }
}
