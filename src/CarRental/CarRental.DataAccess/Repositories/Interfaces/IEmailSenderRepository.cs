﻿using CarRental.Core.Models;

namespace CarRental.DataAccess.Repositories.Interfaces
{
	public interface IEmailSenderRepository
	{

        Task SendEmailAsync(Message message, bool isHtml = false);
        void SendEmail(Message message, bool isHtml = false);

    }
}
