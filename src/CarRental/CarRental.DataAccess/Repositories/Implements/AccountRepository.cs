﻿using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CarRental.DataAccess.Repositories.Implements
{
    public class AccountRepository : BaseRepository<Account>, IAccountRepository
    {
        public AccountRepository(CarRentalContext carRentalContext) : base(carRentalContext)
        {
        }

        public Account GetAccountByCarId(Guid carId)
        {
            var accountId = carRentalContext.Cars.Where(c => c.CarId == carId).Select(x => x.UserID).FirstOrDefault();
            return carRentalContext.Accounts.FirstOrDefault(x => x.Id == accountId);
        }
    }
}
