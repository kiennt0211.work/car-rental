﻿using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;

namespace CarRental.DataAccess.Repositories.Implements
{
    public class FeedBackRepository : BaseRepository<Feedback>, IFeedBackRepository
    {
        public FeedBackRepository(CarRentalContext carRentalContext) : base(carRentalContext)
        {
        }
    }
}
