﻿using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRental.DataAccess.Repositories.Implements
{
	public class BrandModelRepository : BaseRepository<BrandModel>, IBrandModelRepository
	{
		public BrandModelRepository(CarRentalContext carRentalContext) : base(carRentalContext)
		{
		}

		public async Task<List<string?>> GetBrands()
		{
			var result = await entities.Select(e => e.Brand).Distinct().ToListAsync();

			return result;
		}

		public async Task<List<string?>> GetModelsByBrand(string selectedBrand)
		{
			var result = await entities
				.Where(x => x.Brand == selectedBrand)
				.Select(x => x.Model).ToListAsync();
			return result;
		}
	}
}
