﻿using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

public class BaseRepository<T> : IBaseRepository<T> where T : class
{
    protected readonly CarRentalContext carRentalContext;
    protected readonly DbSet<T> entities;

    public BaseRepository(CarRentalContext carRentalContext)
    {
        this.carRentalContext = carRentalContext;
        entities = carRentalContext.Set<T>();
    }

    // Create new record into database
    public void Add(T entity)
    {
        entities.Add(entity);
    }

    // Delete a record with parameter is an object
    public void Delete(T entity)
    {
        entities.Remove(entity);
    }

    // Find record with parameter is Id
    public T? FindByID(object id)
    {
        return entities.Find(id);
    }

    // Get all records from model T in database
    public List<T> GetAll()
    {
        return entities.ToList();
    }

    // Modify a record and update into database
    public void Update(T entity)
    {
        entities.Update(entity);
    }

    // Config for Include
    public virtual IEnumerable<T> GetInclude(Expression<Func<T, bool>> filter, params string[] includeProps)
    {
        var query = entities.AsQueryable();
        if (includeProps.Length > 0)
        {
            foreach (var prop in includeProps)
            {
                query = query.Include(prop);
            }
        }
        return query.Where(filter).ToList();
    }

    public async Task SaveChangeAsync()
    {
        await carRentalContext.SaveChangesAsync();
    }

    public Task<T?> FirstOrDefault(Expression<Func<T, bool>> filter)
    {
        return entities.FirstOrDefaultAsync(filter);
    }

}