﻿using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using MimeKit;
using MimeKit.Text;

namespace CarRental.DataAccess.Repositories.Implements
{
    public class SendMailRepository : IEmailSenderRepository
    {
        private readonly EmailConfiguration _emailConfig;

        public SendMailRepository(EmailConfiguration emailConfig)
        {
            _emailConfig = emailConfig;
        }

        private MimeMessage CreateEmailMessage(Message message, bool ishtml)
        {
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress("email", _emailConfig.From));
            emailMessage.To.AddRange(message.To);
            emailMessage.Subject = message.Subject;
            TextPart textPart;
            if (ishtml)
            {
                textPart = new TextPart(TextFormat.Html)
                {
                    Text = message.Content
                };
            }
            else
            {
                textPart = new TextPart(TextFormat.Plain)
                {
                    Text = message.Content
                };
            }
            emailMessage.Body = textPart;

            return emailMessage;

        }
        private void Send(MimeMessage emailMessage)
        {
            using var client = new MailKit.Net.Smtp.SmtpClient();
            try
            {
                client.Connect(_emailConfig.SmtpServer, _emailConfig.Port, true);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate(_emailConfig.UserName, _emailConfig.Password);

                client.Send(emailMessage);
            }
            catch (Exception ex)
            {
                // Handle the exception here.
                throw;
            }
            finally
            {
                client.DisconnectAsync(true);
                client.Dispose();
            }
        }

        private async Task SendAsync(MimeMessage mailMessage)
        {
            using var client = new MailKit.Net.Smtp.SmtpClient();
            try
            {
                await client.ConnectAsync(_emailConfig.SmtpServer, _emailConfig.Port, true);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                await client.AuthenticateAsync(_emailConfig.UserName, _emailConfig.Password);
                await client.SendAsync(mailMessage);
            }
            catch
            {
                //log an error message or throw an exception, or both.
                throw;
            }
            finally
            {
                await client.DisconnectAsync(true);
                client.Dispose();
            }
        }



        public async Task SendEmailAsync(Message message, bool isHtml = false)
        {
            var mailMessage = CreateEmailMessage(message, isHtml);

            await SendAsync(mailMessage);
        }

        public void SendEmail(Message message, bool isHtml = false)
        {
            var emailMessage = CreateEmailMessage(message, isHtml);

            Send(emailMessage);
        }
    }
}
