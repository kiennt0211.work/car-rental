﻿using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using DocumentFormat.OpenXml.InkML;
using Microsoft.EntityFrameworkCore;

namespace CarRental.DataAccess.Repositories.Implements
{
    public class CarRepository : BaseRepository<Car>, ICarRepository
    {
        public CarRepository(CarRentalContext carRentalContext) : base(carRentalContext)
        {
        }

        public List<Car> GetCarsByUserId(Guid userId)
        {
            return entities.Where(car => car.UserID == userId).ToList();
        }

        public List<Car> GetCarsByDate(DateTime pickupDate, DateTime dropOffDate, string? pickupLocation)
        {
            var availableCars = entities
            .Where(car =>
            (string.IsNullOrEmpty(pickupLocation)
            || car.Address.City.Contains(pickupLocation)
            || car.Address.District.Contains(pickupLocation)
            || car.Address.Ward.Contains(pickupLocation))
            && car.CarStatus == Core.Enums.CarStatus.Available)
            .Include(x => x.CarImage)
            .Include(x => x.Address)
            .ToList();

            return availableCars;
        }

        public List<Car> GetCarsByCity(string city)
        {
            return entities.Where(car => car.Address.City == city)
            .Include(x => x.CarImage)
            .Include(x => x.Address)
            .ToList();
        }

        public List<(string City, int CarCount)> GetTopCitiesWithMostCars()
        {
            var carCountsByCity = entities
                        .Include(x => x.Address)
                        .ToList()
                        .GroupBy(c => c.Address.City)
                        .Select(g => (City: g.Key, CarCount: g.Count()))
                        .OrderByDescending(c => c.CarCount)
                        .Take(6)
                        .ToList();

            return carCountsByCity.Select(x => (x.City!, x.CarCount)).ToList();
        }

        public async Task<List<Feedback>> GetFeedbacksByStar(string filter, Account user)
        {
            var feedbacks = carRentalContext.Feedbacks
                .Where(x => x.Booking.Car.UserID == user.Id)
                .Include(x => x.Booking)
                .ThenInclude(x => x.Car)
                .ThenInclude(x => x.CarImage)
                .AsQueryable();

            if (filter != "All")
            {
                switch (filter)
                {
                    case "1": feedbacks = feedbacks.Where(x => x.Ratings == 1); break;
                    case "2": feedbacks = feedbacks.Where(x => x.Ratings == 2); break;
                    case "3": feedbacks = feedbacks.Where(x => x.Ratings == 3); break;
                    case "4": feedbacks = feedbacks.Where(x => x.Ratings == 4); break;
                    case "5": feedbacks = feedbacks.Where(x => x.Ratings == 5); break;
                }
            }

            return await feedbacks.ToListAsync();

        }

        public double GetAverageStarForUser(Account user)
        {
            //var averageStar = 0;
            var averageStar = carRentalContext.Feedbacks
                 .Where(x => x.Booking.Car.UserID == user.Id)
                 .Select(x => x.Ratings)
                 .Average();
            return (double)averageStar;
        }
    }
}
