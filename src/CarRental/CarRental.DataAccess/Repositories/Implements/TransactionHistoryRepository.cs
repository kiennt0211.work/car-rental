﻿using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CarRental.DataAccess.Repositories.Implements
{
    public class TransactionHistoryRepository : BaseRepository<TransactionHistory>, ITransactionHistoryRepository
    {
        public TransactionHistoryRepository(CarRentalContext carRentalContext) : base(carRentalContext)
        {
        }

        public List<TransactionHistory> ListTransactionsByUserId(string userId)
        {
            return entities.Where(x => x.UserId == userId)
                .Include(x => x.Booking)
                .Include(x => x.User)
                .OrderByDescending(x => x.TransactionDate)
                .ToList();
        }
    }
}
