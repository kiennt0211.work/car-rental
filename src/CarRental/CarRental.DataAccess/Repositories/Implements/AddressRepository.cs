﻿using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CarRental.DataAccess.Repositories.Implements
{
    public class AddressRepository : BaseRepository<Address>, IAddressRepository
    {
        public AddressRepository(CarRentalContext carRentalContext) : base(carRentalContext)
        {
        }
    }
}
