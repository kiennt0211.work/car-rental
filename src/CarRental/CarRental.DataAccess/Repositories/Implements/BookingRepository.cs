﻿using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CarRental.DataAccess.Repositories.Implements
{
    public class BookingRepository : BaseRepository<Booking>, IBookingRepository
    {

        public BookingRepository(CarRentalContext carRentalContext) : base(carRentalContext)
        {
        }

        public async Task<Booking> CancelBooking(Guid bookingId)
        {
            var booking = await carRentalContext.Bookings.FirstOrDefaultAsync(b => b.BookingId == bookingId);
            if (booking != null)
            {
                booking.Status = "4";
                carRentalContext.Entry(booking).State = EntityState.Modified;
                await carRentalContext.SaveChangesAsync();
                return booking;
            }

            return booking;
        }
        public async Task<Booking> ConfirmPickup(Guid bookingId)
        {
            var booking = await carRentalContext.Bookings.FirstOrDefaultAsync(b => b.BookingId == bookingId);
            if (booking != null)
            {
                booking.Status = "2";
                carRentalContext.Entry(booking).State = EntityState.Modified;
                await carRentalContext.SaveChangesAsync();

                return booking;
            }

            return booking;
        }

        public List<Booking> GetBookingsForCustomer(Guid userId, int page, int pageSize)
        {
            var bookings = entities
                .Where(b => b.UserId == userId)
                .Include(x => x.Car)
                .ThenInclude(x => x.CarImage)
                .OrderByDescending(b => b.BookingId)
                .Skip((page - 1) * pageSize)
                .Take(pageSize)
                .ToList();

            return bookings;
        }


        public Car GetCarByBookingId(Guid bookingId)
        {
            return entities
                .Where(booking => booking.BookingId == bookingId)
                .Include(booking => booking.Car.BrandModel)
                .Include(booking => booking.Car.Color)
                .Include(booking => booking.Car.CarImage)
                .Include(booking => booking.Car.Address)
                .Select(booking => booking.Car)
                .FirstOrDefault();
        }

        public async Task<Booking> RateCar(Guid bookingId, int ratings, string content)
        {
            var booking = await carRentalContext.Bookings.FirstOrDefaultAsync(b => b.BookingId == bookingId);
            if (booking != null)
            {
                // Tạo một đối tượng Feedback từ dữ liệu được gửi từ modal.
                var feedback = new Feedback
                {
                    BookingId = bookingId,
                    Ratings = ratings,
                    Content = content
                };

                // Thêm phản hồi vào cơ sở dữ liệu và lưu thay đổi.
                carRentalContext.Feedbacks.Add(feedback);
                await carRentalContext.SaveChangesAsync();
                return booking;
            }
            return booking;
        }

        public int CountBooking(Guid carId)
        {
            return entities.Count(booking => booking.CarId == carId);
        }
    }
}