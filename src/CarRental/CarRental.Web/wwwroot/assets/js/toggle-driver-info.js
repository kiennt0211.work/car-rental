const checkbox = document.getElementById('diff');
const driverInfo = document.getElementById('driverInfo');

const renterFullNameInput = document.getElementById("validationCustom01");
const renterDobInput = document.getElementById("validationCustom02");
const renterPhoneNumberInput = document.getElementById("validationCustom03");
const renterEmailInput = document.getElementById("validationCustom04");
const renterNationIdNumberInput = document.getElementById("validationCustom05");
const renterDrivingLicenseInput = document.getElementById("validationCustom06");
const renterSpecificAddressInput = document.getElementById("validationCustom07");

const driverFullNameInput = document.getElementById("validationCustom08");
const driverDobInput = document.getElementById("validationCustom09");
const driverPhoneNumberInput = document.getElementById("validationCustom10");
const driverEmailInput = document.getElementById("validationCustom11");
const driverNationIdNumberInput = document.getElementById("validationCustom12");
const driverDrivingLicenseInput = document.getElementById("validationCustom13");
const driverSpecificAddressInput = document.getElementById("validationCustom14");

checkbox.addEventListener('change', function () {
    if (checkbox.checked) {
        driverInfo.style.display = 'block';
        driverFullNameInput.disabled = false;
        driverFullNameInput.value = "";
        driverDobInput.disabled = false;
        driverDobInput.value = "";
        driverPhoneNumberInput.disabled = false;
        driverPhoneNumberInput.value = "";
        driverEmailInput.disabled = false;
        driverEmailInput.value = "";
        driverNationIdNumberInput.disabled = false;
        driverNationIdNumberInput.value = "";
        driverDrivingLicenseInput.disabled = false;
        driverDrivingLicenseInput.value = "";
        driverSpecificAddressInput.disabled = false;
        driverSpecificAddressInput.value = "";

    } else {
        driverInfo.style.display = 'none';
        driverFullNameInput.value = renterFullNameInput.value;
        driverFullNameInput.disabled = true;
        driverDobInput.value = renterDobInput.value;
        driverDobInput.disabled = true;
        driverPhoneNumberInput.disabled = true;
        driverPhoneNumberInput.value = renterPhoneNumberInput;
        driverEmailInput.disabled = true;
        driverEmailInput.value = renterEmailInput;
        driverNationIdNumberInput.disabled = true;
        driverNationIdNumberInput.value = renterNationIdNumberInput;
        driverDrivingLicenseInput.disabled = true;
        driverDrivingLicenseInput.value = renterDrivingLicenseInput;
        driverSpecificAddressInput.disabled = true;
        driverSpecificAddressInput.value = renterSpecificAddressInput;
    }
});