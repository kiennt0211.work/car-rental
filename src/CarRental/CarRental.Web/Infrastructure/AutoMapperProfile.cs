﻿using AutoMapper;
using CarRental.Core.Models;
using CarRental.Web.Areas.Accounts.ViewModel;
using CarRental.Web.Areas.CarOwner.ViewModels;
using CarRental.Web.Areas.Customer.Models;

namespace SelfProjectMVC.Infrastructure
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Car, CarModel>().ReverseMap();
            CreateMap<Booking, BookingModel>().ReverseMap();

            CreateMap<Car,AddCarViewModel>().ReverseMap();

            CreateMap<Car,EditCarViewModel>().ReverseMap();
            CreateMap<Account,UserEditProfileModel>().ReverseMap();
            CreateMap<Address, UserEditProfileModel>().ReverseMap();
            CreateMap<Feedback, FeedBackViewModel>().ReverseMap();
            CreateMap<FeedBackViewModel, Feedback>().ReverseMap();
        }
    }
}
