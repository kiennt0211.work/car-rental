﻿using CarRental.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace CarRental.Web.Areas.Accounts.ViewModel
{
    public class UserEditProfileModel
    {
        [Required]
        public string FullName { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string? PhoneNumber { get; set; }

        public string? NationalIdNo { get; set; }

        public Address? Address { get; set; }

        public int? AddressId { get; set; }
        public string? SpecificAddress { get; set; }
        public DateTime DateOfBirth { get; set; }

        [DataType(DataType.EmailAddress)]
        public string? EmailAddress { get; set; }

        public IFormFile? ImageFileDriverLicense { get; set; }

        public string? ImageUrlDriverLicense { get; set; }
        public string? Description { get; set; }

    }
}
