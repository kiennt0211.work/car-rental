﻿using System.ComponentModel.DataAnnotations;

namespace CarRental.Web.Areas.Accounts.ViewModel
{
    public class ForgotPasswordModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
