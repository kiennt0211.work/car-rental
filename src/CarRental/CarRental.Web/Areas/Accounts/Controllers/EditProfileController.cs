﻿using AutoMapper;
using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using CarRental.Web.Areas.Accounts.ViewModel;
using CarRental.Web.Areas.Customer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CarRental.Web.Areas.Accounts.Controllers
{
    [Area("Accounts")]
    [Authorize]

    public class EditProfileController : Controller
    {
        private readonly SignInManager<Account> _signInManager;
        private readonly UserManager<Account> _userManager;
        private readonly IWebHostEnvironment _host;
        private readonly IMapper _mapper;
        private readonly IAddressRepository _addressRepository;
        private readonly IBrandModelRepository _brandModelRepository;

        public EditProfileController(SignInManager<Account> signInManager, UserManager<Account> userManager, IWebHostEnvironment host, IMapper mapper, IAddressRepository addressRepository, IBrandModelRepository brandModelRepository)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _host = host;
            _mapper = mapper;
            _addressRepository = addressRepository;
            _brandModelRepository = brandModelRepository;
        }

        public async Task<IActionResult> EditProfile(string? username)
        {
            if (username == null)
            {
                return BadRequest("Username cannot be null");
            }

            var user = await _userManager.FindByNameAsync(username);

            if (user == null)
            {
                return NotFound();
            }

            UserEditProfileModel userEdit = new UserEditProfileModel()
            {

                DateOfBirth = user.DateOfBirth,
                AddressId = user.AddressID ?? 0,
                EmailAddress = user.Email,
                ImageUrlDriverLicense = user.DrivingLicense,
                NationalIdNo = user.NationalID,
                PhoneNumber = user.PhoneNumber,
                FullName = user.FullName ?? "",
                Description = user.SpecificAddress
            };

            if (user.AddressID == null)
            {
                var data = _addressRepository.GetAll();
                if (data != null)
                {
                    //userEdit.Address = data;
                    ViewBag.Cities = _addressRepository.GetAll()
                        .OrderBy(a => a.City)
                        .Select(a => a.City)
                        .Distinct()
                        .ToList();
                }
            }

            if (user.AddressID.HasValue)
            {
                var address = _addressRepository.FindByID(user.AddressID.Value);
                if (address != null)
                {
                    userEdit.Address = address;
                    ViewBag.Cities = _addressRepository.GetAll()
                        .OrderBy(a => a.City)
                        .Select(a => a.City)
                        .Distinct()
                        .ToList();
                }
            }
            return View(userEdit);
        }

        [HttpPost]
        public async Task<IActionResult> EditProfile(string? username, UserEditProfileModel editUser)
        {
            if (username == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByNameAsync(username);

            if (user == null)
            {
                return NotFound();
            }

            if (editUser.ImageFileDriverLicense != null)
            {
                editUser.ImageUrlDriverLicense = UploadProfileImage(editUser.ImageFileDriverLicense);
            }

            var address = await _addressRepository.FirstOrDefault(
                filter: x => editUser.Address != null
                && x.City == editUser.Address.City
                && x.District == editUser.Address.District
                && x.Ward == editUser.Address.Ward
            );

            if (address != null)
            {
                user.Address = address;
                user.AddressID = address.AddressId;
            }

            user.FullName = editUser.FullName;
            user.PhoneNumber = editUser.PhoneNumber;
            user.NationalID = editUser.NationalIdNo;
            user.DateOfBirth = editUser.DateOfBirth;
            user.Email = editUser.EmailAddress;
            user.DrivingLicense = editUser.ImageUrlDriverLicense;
            user.SpecificAddress = editUser.SpecificAddress;


            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                ViewBag.success = "Edit user successfully!";
            }
            else
            {
                ViewBag.error = "Edit user failure!";
            }

            return View(editUser);
        }

        [HttpGet]
        public JsonResult GetCities()
        {
            var cities = _addressRepository.GetAll().Select(a => a.City).Distinct().ToList();

            return Json(cities);
        }

        [HttpGet]
        public JsonResult GetDistricts(string selectedCity)
        {
            var districts = _addressRepository.GetAll()
                .Where(a => a.City == selectedCity)
                .Select(a => a.District)
                .Distinct()
                .ToList();

            return Json(districts);
        }

        [HttpGet]
        public JsonResult GetWards(string selectedDistrict)
        {
            var wards = _addressRepository.GetAll()
                .Where(a => a.District == selectedDistrict)
                .Select(a => a.Ward)
                .Distinct()
                .ToList();

            return Json(wards);
        }

        public string UploadProfileImage(IFormFile file)
        {
            if (file != null)
            {
                var root = _host.WebRootPath;
                var carImagesPath = Path.Combine(root, "profile-images");

                // Kiểm tra xem thư mục đã tồn tại chưa, nếu chưa thì tạo mới
                if (!Directory.Exists(carImagesPath))
                {
                    Directory.CreateDirectory(carImagesPath);
                }

                var prefix = Guid.NewGuid().ToString().ToUpper().Split('-')[0];
                var filePath = Path.Combine(carImagesPath, $"{prefix}-{file.FileName}");
                using (var stream = System.IO.File.Create(filePath))
                {
                    file.CopyTo(stream);
                }
                return $"/profile-images/{prefix}-{file.FileName}";
            }

            return null; // hoặc một giá trị mặc định khác tùy vào yêu cầu của bạn
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(User);

                if (user != null)
                {
                    var result = await _userManager.ChangePasswordAsync(user, model.CurrentPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        // Đăng xuất người dùng
                        await _signInManager.SignOutAsync();
                        TempData["success"] = "Changed password successfully! Please sign in again.";

                        return Json(new
                        {
                            success = true,
                            redirect = Url.Action("Login", "Account", new { area = "Accounts" }),
                        });
                    }
                    else
                    {
                        // Xử lý lỗi từ UserManager
                        var errorList = result.Errors.Select(e => e.Description).ToList();
                        return Json(new { success = false, errors = errorList });
                    }

                }
            }
            var modelErrors = ModelState.Values
            .SelectMany(v => v.Errors)
            .Select(e => e.ErrorMessage)
            .ToList();

            return Json(new { success = false, errors = modelErrors });
        }

    }
}
