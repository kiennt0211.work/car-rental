﻿using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;

namespace CarRental.Web.Areas.Accounts.Controllers
{
    [Area("Accounts")]
    public class WalletController : Controller
    {
        private readonly UserManager<Account> userManager;
        private readonly ITransactionHistoryRepository transactionHistoryRepository;
        private readonly IToastNotification _toastNotification;
        public WalletController(UserManager<Account> userManager, IToastNotification toastNotification, ITransactionHistoryRepository transactionHistoryRepository)
        {
            this.userManager = userManager;
            this._toastNotification = toastNotification;
            this.transactionHistoryRepository = transactionHistoryRepository;
        }
        public async Task<IActionResult> MyWallet()
        {
            var currentUser = await userManager.GetUserAsync(User);
            ViewBag.Wallet = currentUser.Wallet;
            var transactions = transactionHistoryRepository.ListTransactionsByUserId(currentUser.Id.ToString());
            return View(transactions);
        }
        public async Task<IActionResult> Topup(decimal topup_amount)
        {
            var currentUser = await userManager.GetUserAsync(User);
            currentUser.Wallet += topup_amount;
            var transaction = new TransactionHistory
            {
                Id = Guid.NewGuid(),
                UserId = currentUser.Id.ToString(),
                Amount = topup_amount,
                TransactionDate = DateTime.Now,
                TransactionType = "Top up",
                TransactionDescription = ""
            };
            await userManager.UpdateAsync(currentUser);
            transactionHistoryRepository.Add(transaction);
            await transactionHistoryRepository.SaveChangeAsync();
            _toastNotification.AddSuccessToastMessage("Top up successfully!! Thank you");
            var transactions = transactionHistoryRepository.ListTransactionsByUserId(currentUser.Id.ToString());
            return RedirectToAction("MyWallet", transactions);
        }
        public async Task<IActionResult> Withdraw(decimal withdraw_amount)
        {
            var currentUser = await userManager.GetUserAsync(User);
            currentUser.Wallet -= withdraw_amount;
            var transaction = new TransactionHistory
            {
                Id = Guid.NewGuid(),
                UserId = currentUser.Id.ToString(),
                Amount = -withdraw_amount,
                TransactionDate = DateTime.Now,
                TransactionType = "Withdraw",
                TransactionDescription = ""
            };
            await userManager.UpdateAsync(currentUser);
            transactionHistoryRepository.Add(transaction);
            await transactionHistoryRepository.SaveChangeAsync();
            _toastNotification.AddSuccessToastMessage("Withdraw successfully!! Thank you");
            var transactions = transactionHistoryRepository.ListTransactionsByUserId(currentUser.Id.ToString());
            return RedirectToAction("MyWallet", transactions);
        }
    }
}