﻿using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using CarRental.Web.Areas.Accounts.ViewModel;
using CarRental.Web.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;

namespace CarRental.Web.Areas.Accounts.Controllers
{
	[Area("Accounts")]
	public class AccountController : Controller
	{
		private readonly SignInManager<Account> _signInManager;
		private readonly ILogger<AccountController> _logger;
		private readonly UserManager<Account> _userManager;
		private readonly IUserStore<Account> _userStore;
        private readonly IEmailSenderRepository _emailSender;
        private readonly IToastNotification _toastNotification;

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public AccountController(
			SignInManager<Account> signInManager,
			ILogger<AccountController> logger,
			UserManager<Account> userManager,
			IUserStore<Account> userStore,
			IEmailSenderRepository emailSender,
			IToastNotification toastNotification)
		{
			_signInManager = signInManager;
			_logger = logger;
			_userManager = userManager;
			_userStore = userStore;
			_emailSender = emailSender;
            _toastNotification = toastNotification;
        }

		[HttpGet]
		public async Task<IActionResult> Login(string returnUrl = null)
		{
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            returnUrl ??= Url.Content("~/");

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            ReturnUrl = returnUrl;
            return View();
        }

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Login(LoginModel model)
		{
            string returnUrl = Url.Content("~/");

            //ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
			{
				var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
				if (result.Succeeded)
				{
					var user = await _userManager.FindByNameAsync(model.Email);
					if(!user.IsActive)
					{
                        
                        await _signInManager.SignOutAsync();
                        _toastNotification.AddWarningToastMessage("This Account has been banned! Contact to the Admin for more detail!");
                        return Redirect(Url.Content("~/Accounts/Account/Login"));
                    }
					else
					{


                        if (await _userManager.IsInRoleAsync(user, ListRoles.CarOwner))
                        {
                            _logger.LogInformation("User logged in with role CarOwner");
                            return Redirect(Url.Content("~/CarOwner/Home/Index"));
                        }

                        if (await _userManager.IsInRoleAsync(user, ListRoles.Customer))
                        {
                            _toastNotification.AddSuccessToastMessage("Login successfully!! Welcome to our website");
                            _logger.LogInformation("User logged in with role customer");
                            return Redirect(Url.Content("~/Customer/Home/Index"));
                        }
                        if (await _userManager.IsInRoleAsync(user, ListRoles.Admin))
                        {
                            _toastNotification.AddSuccessToastMessage("Login successfully!! You Logged In as a Administrator");
                            _logger.LogInformation("User logged in with role customer");
                            return Redirect(Url.Content("~/Admin/Home/Index"));
                        }
                    }
				}
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    TempData["error"] = "Invalid login attempt.";
                    return View(model);


                }

            }
            return View(model);
		}

		[HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout(string returnUrl = null)
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            if (returnUrl != null)
            {
                return View("/Views/Home/Index.cshtml");
            }
            else
            {
                return View();
            }
        }

		public async Task<IActionResult> Register(string returnUrl = null)
		{
			ReturnUrl = returnUrl;
			ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
			return View();
		}

		[HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
		{
			string returnUrl = Url.Content("~/");
			if (ModelState.IsValid)
			{
				var user = CreateUser();
				user.Email = model.Email;
				user.FullName = model.FullName;
				user.PhoneNumber = model.Phone;

				await _userStore.SetUserNameAsync(user, model.Email, CancellationToken.None);
				var result = await _userManager.CreateAsync(user, model.Password);

				var setUserRole = HttpContext.Request.Form["userRole"];
				if (setUserRole == "customer")
				{
                    await _userManager.AddToRoleAsync(user, ListRoles.Customer);
				}
				else if (setUserRole == "carOwner")
				{
                    await _userManager.AddToRoleAsync(user, ListRoles.CarOwner);
				}
				if (result.Succeeded)
				{


					_logger.LogInformation("User created a new account with password.");

					var userId = await _userManager.GetUserIdAsync(user);

					await _signInManager.SignInAsync(user, isPersistent: false);

					if (await _userManager.IsInRoleAsync(user, ListRoles.Customer))
					{
                        _toastNotification.AddSuccessToastMessage("Register successfully!! You have successfully registered");

                        return Redirect(Url.Content("~/Customer/Home/Index"));
					}
					else if (await _userManager.IsInRoleAsync(user, ListRoles.CarOwner))
					{
						_toastNotification.AddSuccessToastMessage("Register successfully!! You have successfully registered");

                        return Redirect(Url.Content("~/CarOwner/Home/Index"));
					}
				}
				foreach (var error in result.Errors)
				{
					ModelState.AddModelError(string.Empty, error.Description);
				}
			}

			return View();
		}

		private Account CreateUser()
		{
			try
			{
				return Activator.CreateInstance<Account>();
			}
			catch
			{
				throw new InvalidOperationException($"Can't create an instance of '{nameof(CarRental.Core.Models.Account)}'. " +
					$"Ensure that '{nameof(CarRental.Core.Models.Account)}' is not an abstract class and has a parameterless constructor, or alternatively " +
					$"override the register page in /Areas/Identity/Pages/Account/Register.cshtml");
			}
		}

		private IUserEmailStore<Account> GetEmailStore()
		{
			if (!_userManager.SupportsUserEmail)
			{
				throw new NotSupportedException("The default UI requires a user store with email support.");
			}
			return (IUserEmailStore<Account>)_userStore;
		}

        public IActionResult ResetPassword(string token, string email)
        {
            var model = new ResetPasswordModel { Token = token, Email = email };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel resetPasswordModel)
        {
            if (!ModelState.IsValid)
            {
                return View(resetPasswordModel);
            }

            var user = await _userManager.FindByEmailAsync(resetPasswordModel.Email);

            if (user == null)
            {
                return NotFound();
            }

            var resetPassResult = await _userManager.ResetPasswordAsync(user, resetPasswordModel.Token, resetPasswordModel.Password);

            if (!resetPassResult.Succeeded)
            {
                foreach (var error in resetPassResult.Errors)
                {
                    ModelState.TryAddModelError(error.Code, error.Description);
                }
                return View();
            }
            ViewBag.success = "Change password successfully!";
            return RedirectToAction(nameof(ResetPasswordConfirmation));
        }

        public async Task<IActionResult> ForgotPassword()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> ForgotPassword(ForgotPasswordModel forgotPasswordModel)
		{
			if (!ModelState.IsValid)
			{
				return View(forgotPasswordModel);
			}

			var user = await _userManager.FindByEmailAsync(forgotPasswordModel.Email);

			if (user == null)
			{
				ModelState.AddModelError("User not found", "Cannot found user email");
				return View(forgotPasswordModel);
			}
			else
			{
				var token = await _userManager.GeneratePasswordResetTokenAsync(user);
				var callback = Url.Action(nameof(ResetPassword), "Account", new { token, email = user.Email }, Request.Scheme);
				var message = new Message(
					new string[] { user.Email },
					"Reset password",
					EmailTemplate.ForgotPasswordEmail(callback, user.FullName),
					null);
				await _emailSender.SendEmailAsync(message);
				return RedirectToAction(nameof(ForgotPasswordConfirmation));
			}
		}

		public IActionResult ForgotPasswordConfirmation()
		{
			return View();
		}
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }
    }
}
