﻿using AutoMapper;
using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using CarRental.Web.Areas.Customer.Models;
using DocumentFormat.OpenXml.Drawing.Charts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NToastNotify;
using PagedList;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;

namespace CarRental.Web.Areas.Customer.Controllers
{
    [Area("Customer")]
    [Authorize(Roles = ListRoles.Customer)]
    public class BookingController : Controller
    {
        private readonly IBookingRepository _bookingRepository;
        private readonly IMapper _mapper;
        private readonly ICarRepository _carRepository;
        private readonly IAccountRepository accountRepository;
        private readonly UserManager<Account> userManager;
        private readonly IToastNotification _toastNotification;
        private readonly ITransactionHistoryRepository transactionHistoryRepository;
		private readonly IAddressRepository addressRepository;

		public BookingController(IBookingRepository bookingRepository, IMapper mapper, ICarRepository carRepository, IAccountRepository accountRepository,
            UserManager<Account> userManager, IToastNotification toastNotification,
            ITransactionHistoryRepository transactionHistoryRepository,
            IAddressRepository addressRepository)
        {
            _bookingRepository = bookingRepository;
            _mapper = mapper;
            _carRepository = carRepository;
            this.userManager = userManager;
            _toastNotification = toastNotification;
            this.accountRepository = accountRepository;
            this.transactionHistoryRepository = transactionHistoryRepository;
			this.addressRepository = addressRepository;
		}

        /// <summary>
        /// view list booking of customer
        /// </summary>
        /// <param name="page">paging list</param>
        /// <returns>lists booking</returns>
        public async Task<IActionResult> Index(int? page)
        {
            var currentUser = await userManager.GetUserAsync(User);

            if (currentUser == null)
            {
                return RedirectToAction("Error");
            }

            int pageNumber = page ?? 1;
            int pageSize = 6;
            var bookings = _bookingRepository.GetBookingsForCustomer(currentUser.Id, pageNumber, pageSize);

            return View(bookings.ToPagedList(pageNumber, pageSize));
        }


        /// <summary>
        /// view detail booking car
        /// </summary>
        /// <param name="id">param id to fuction which to checked match with bookingId </param>
        /// <returns>detail booking car</returns>
        public async Task<IActionResult> Details(Guid id)
        {
            var currentUser = await userManager.GetUserAsync(User);
            var userIdClaim = currentUser.Id;



            if (userIdClaim == null)
            {
                return RedirectToAction("Error");
            }
            var booking = _bookingRepository.GetInclude(c => c.BookingId == id, "Car", "Account").FirstOrDefault();

            booking.Account = accountRepository.FindByID(booking.UserId);
            booking.RenterAddress = addressRepository.FindByID(booking.Account.AddressID);
            if(booking.DriverAddress == null)
            {
                booking.DriverAddress = booking.RenterAddress;
            }
            booking.RenterDob = currentUser.DateOfBirth;
            if(booking.DriverDob == null)
            {
				booking.DriverDob = currentUser.DateOfBirth;
			}
            ViewBag.RenterDateOfBirth = booking.RenterDob;
			ViewBag.DriverDateOfBirth = booking.DriverDob;

			var carDetails = _bookingRepository.GetCarByBookingId(id);
            ViewBag.CarDetails = carDetails;
			if (!carDetails.TermsOfUse.Contains("other", StringComparison.CurrentCultureIgnoreCase))
			{
				ViewBag.SpecificOther = "";
			}
			else
			{

				int index = carDetails.TermsOfUse.IndexOf("other");
				if (index > -1)
				{
					var TempString = carDetails.TermsOfUse.Substring(index + "other".Length);
					ViewBag.SpecificOther = TempString.Substring(1);

				}
				else
				{
					ViewBag.SpecificOther = "";
				}
			}
			if (booking == null)
            {
                return NotFound();
            }
            return View(booking);
        }
        /// <summary>
        /// body send email for cancel booking
        /// </summary>
        /// <returns>view body</returns>
		public string BodyCancelOrderMail(Booking booking)
        {
            string body = string.Empty;
            string car = "";
            DateTime timeCancel = DateTime.Now;
            using (StreamReader reader = new StreamReader(Path.Combine(Directory.GetCurrentDirectory(), "Areas\\Customer\\Views\\Shared\\Mail", "CancelBooking.cshtml")))
            {
                body = reader.ReadToEnd();
            }
            car += "<tr>";
            car += "<td width = '65 % ' align='left' style='font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 15px 10px 5px 10px; '>";
            car += booking.Car.CarName + "</td>";
            car += "<td width = '10 % ' align='left' style='font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 15px 10px 5px 10px; '>";
            car += booking.NumberOfDays + "</td>";
            car += "<td width = '25 % ' align='left' style='font-family: Open Sans, Helvetica, Arial, sans-serif; font-size: 16px; font-weight: 400; line-height: 24px; padding: 15px 10px 5px 10px; '>";
            car += string.Format("{0:N0} VND", booking.Car.BasePrice) + "</td>";
            car += "</tr>";

            body = body.Replace("{{StartDate}}", booking.StartDateTime.ToShortDateString());
            body = body.Replace("{{EndDate}}", booking.EndDateTime.ToShortDateString());
            body = body.Replace("{{TimeCancel}}", timeCancel.ToShortDateString());
            body = body.Replace("{{Car}}", car);
            body = body.Replace("{{Tilte}}", booking.BookingId.ToString());
            body = body.Replace("{{ToTalPrice}}", string.Format("{0:N0} VND", booking.Total));
           
            return body;
        }

        /// <summary>
        /// function to post from modal cancel boooking 
        /// </summary>
        /// <param name="bookingId">param bookingId</param>
        /// <returns>view list booking</returns>
		[HttpPost]
        public async Task<IActionResult> CancelBooking(Guid bookingId)
        {
            var booking = await _bookingRepository.CancelBooking(bookingId);
            var carDetails = _bookingRepository.GetCarByBookingId(bookingId);
            var currentUser = await userManager.GetUserAsync(User);
            var carOwner = accountRepository.GetAccountByCarId(carDetails.CarId);
            currentUser.Wallet += carDetails.Deposit;
            var customerTransaction = new TransactionHistory
            {
                Id = Guid.NewGuid(),
                UserId = currentUser.Id.ToString(),
                BookingId = booking.BookingId,
                Amount = -carDetails.Deposit,
                TransactionDate = DateTime.Now,
                TransactionType = "Cancel booking",
                TransactionDescription = ""
            };

            carOwner.Wallet -= carDetails.Deposit;
            var carOwnerTransaction = new TransactionHistory
            {
                Id = Guid.NewGuid(),
                UserId = carOwner.Id.ToString(),
                BookingId = booking.BookingId,
                Amount = carDetails.Deposit,
                TransactionDate = DateTime.Now,
                TransactionType = "Cancel booking",
                TransactionDescription = ""
            };
            transactionHistoryRepository.Add(customerTransaction);
            transactionHistoryRepository.Add(carOwnerTransaction);
            if (booking == null)
            {
                return NotFound();
            }

            EmailModel model = new EmailModel()
            {
                Subject = "Cancel booking",
                To = User.FindFirstValue(ClaimValueTypes.Email),
            };

            using (MailMessage mm = new MailMessage(model.From, model.To))
            {
                mm.Subject = model.Subject;
                mm.Body = BodyCancelOrderMail(booking);
                mm.IsBodyHtml = true;
                using (SmtpClient smtp = new SmtpClient())
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(model.From, model.Password);
                    smtp.UseDefaultCredentials = false;
                    smtp.EnableSsl = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mm);
                }
            }
            _toastNotification.AddSuccessToastMessage("Cancel successfully!! Please check your mail");
            return RedirectToAction("Index");
        }


        /// <summary>
        /// function to post from modal cancel confirm pick-up 
        /// </summary>
        /// <param name="bookingId">param bookingId</param>
        /// <returns>view list booking</returns>
        [HttpPost]
        public async Task<IActionResult> ConfirmPickup(Guid bookingId)
        {
            var booking = await _bookingRepository.ConfirmPickup(bookingId);

            if (booking == null)
            {
                return NotFound();
            }
            _toastNotification.AddSuccessToastMessage("Confirm pick-up successfully!!");
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> RateCar(Guid bookingId, int ratings, string content)
        {
            var booking = await _bookingRepository.RateCar(bookingId, ratings, content);

            if (booking == null)
            {
                return NotFound();
            }
            _toastNotification.AddSuccessToastMessage("Give rating car successfully!!");
            return RedirectToAction("Index");
        }


        [HttpPost]
        public async Task<IActionResult> ReturnCar(Guid bookingId)
        {
            // lay user hien tai
            var currentUser = await userManager.GetUserAsync(User);
            var userIdClaim = currentUser.Id;

            if (userIdClaim == null)
            {
                return RedirectToAction("Error");
            }
            var booking = _bookingRepository.GetInclude(c => c.BookingId == bookingId).FirstOrDefault();

            if (booking == null)
            {
                return NotFound();
            }
            booking.Status = "3";
            DateTime currentDate = DateTime.Now;
            var totalPrice = booking.BasePrice * booking.NumberOfDays;
            var totalDeposit = booking.Deposit;
            var carId = _bookingRepository.GetAll().Where(bc => bc.BookingId == bookingId).Select(bc => bc.CarId).FirstOrDefault();
            var carOwner = accountRepository.GetAccountByCarId((Guid)carId);
            if (totalPrice > totalDeposit)
            {
                var currentMoney = totalPrice - totalDeposit;
                currentUser.Wallet -= currentMoney;
                var customerTransaction = new TransactionHistory
                {
                    Id = Guid.NewGuid(),
                    UserId = currentUser.Id.ToString(),
                    BookingId = booking.BookingId,
                    Amount = -currentMoney,
                    TransactionDate = DateTime.Now,
                    TransactionType = "Offset final payment",
                    TransactionDescription = ""
                };
                carOwner.Wallet += currentMoney;
                var carOwnerTransaction = new TransactionHistory
                {
                    Id = Guid.NewGuid(),
                    UserId = carOwner.Id.ToString(),
                    BookingId = booking.BookingId,
                    Amount = currentMoney,
                    TransactionDate = DateTime.Now,
                    TransactionType = "Offset final payment",
                    TransactionDescription = ""
                };
                transactionHistoryRepository.Add(customerTransaction);
                transactionHistoryRepository.Add(carOwnerTransaction);
            }
            else if (totalPrice < totalDeposit)
            {
                var currentMoney = totalDeposit - totalPrice;
                currentUser.Wallet += currentMoney;
                var customerTransaction = new TransactionHistory
                {
                    Id = Guid.NewGuid(),
                    UserId = currentUser.Id.ToString(),
                    BookingId = booking.BookingId,
                    Amount = currentMoney,
                    TransactionDate = DateTime.Now,
                    TransactionType = "Offset final payment",
                    TransactionDescription = ""
                };
                carOwner.Wallet -= currentMoney;
                var carOwnerTransaction = new TransactionHistory
                {
                    Id = Guid.NewGuid(),
                    UserId = carOwner.Id.ToString(),
                    BookingId = booking.BookingId,
                    Amount = -currentMoney,
                    TransactionDate = DateTime.Now,
                    TransactionType = "Offset final payment",
                    TransactionDescription = ""
                };
                transactionHistoryRepository.Add(customerTransaction);
                transactionHistoryRepository.Add(carOwnerTransaction);
            }

            var carUpdateStatus = _carRepository.FindByID(carId);
            carUpdateStatus.CarStatus = Core.Enums.CarStatus.Available;
            await _carRepository.SaveChangeAsync();
            _bookingRepository.Update(booking);
            await _bookingRepository.SaveChangeAsync();
            _toastNotification.AddSuccessToastMessage("Return car successfully!! Thank you");
            return RedirectToAction("Index");
        }
    }
}