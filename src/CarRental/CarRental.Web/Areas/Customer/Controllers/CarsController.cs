﻿using AutoMapper;
using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NToastNotify;
using System.Text;

namespace CarRental.Web.Areas.Customer.Controllers
{
    [Area("Customer")]
    [Authorize(Roles = ListRoles.Customer)]
    public class CarsController : Controller
    {
        private readonly ICarRepository carRepository;
        private readonly IMapper _mapper;
		private readonly IBrandModelRepository brandModelRepository;
        private readonly IAddressRepository addressRepository;
        private readonly IBookingRepository bookingRepository;
        private readonly IAccountRepository accountRepository;
        private readonly ITransactionHistoryRepository transactionHistoryRepository;
        private readonly CarRentalContext _context;
        private readonly UserManager<Account> userManager;
        private readonly IToastNotification _toastNotification;

        public CarsController(ICarRepository carRepository, IMapper mapper, 
            IBrandModelRepository brandModelRepository,
            IAddressRepository addressRepository,
            IBookingRepository bookingRepository,
            IAccountRepository accountRepository,
            UserManager<Account> userManager,
            IToastNotification toastNotification,
            ITransactionHistoryRepository transactionHistoryRepository,
            CarRentalContext context
)
        {
            this.carRepository = carRepository;
            _mapper = mapper;
            this.brandModelRepository = brandModelRepository;
            this.addressRepository = addressRepository;
            this.bookingRepository = bookingRepository;
            this.accountRepository = accountRepository;
            this.transactionHistoryRepository = transactionHistoryRepository;
            _context = context;
            this.userManager = userManager;
            _toastNotification = toastNotification;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SearchCar(DateTime pickupDate, DateTime dropOffDate, string pickupLocation)
        {
            var availableCars = carRepository.GetCarsByDate(pickupDate, dropOffDate, pickupLocation);

            double rentalDays = (dropOffDate - pickupDate).TotalDays;
            TempData["RentalDays"] = rentalDays.ToString();
            TempData["PickupLocation"] = pickupLocation.ToString();
            TempData["PickupDate"] = pickupDate.ToString();
            TempData["DropOffDate"] = dropOffDate.ToString();

            _toastNotification.AddSuccessToastMessage("Search car successfully!! Please rent car .");
            return View(availableCars);
        }

        public IActionResult CityCars(string city)
        {
            var cars = carRepository.GetCarsByCity(city);
            return View(cars);
        }

        public IActionResult ViewCarDetails(Guid carId)
        { 
            var carDetails = carRepository.GetInclude(c => c.CarId == carId, "BrandModel", "Color", "Address","CarImage").SingleOrDefault();

            var ratings = _context.Cars.Include(x => x.Bookings).ThenInclude(x => x.Feedbacks).FirstOrDefault(x => x.CarId == carId);
            var numbersOfRide = bookingRepository.CountBooking(carId);
            ViewBag.NumbersOfRide = numbersOfRide;
            if (ratings != null)
            {
                int ratingSum = (int)ratings.Bookings
                    .SelectMany(booking => booking.Feedbacks)
                    .Sum(feedback => feedback.Ratings);

                int countRatings = ratings.Bookings
                    .SelectMany(booking => booking.Feedbacks)
                    .Count();

                decimal avgRate = countRatings > 0 ? (decimal)ratingSum / countRatings : 0;

                ViewBag.TotalRatings = ratingSum;
                ViewBag.RateCount = countRatings;
                ViewBag.AvgRate = (double)avgRate; 
            }

            if (!carDetails.TermsOfUse.Contains("other", StringComparison.CurrentCultureIgnoreCase))
            {
                ViewBag.SpecificOther = "";
            }
            else
            {

                int index = carDetails.TermsOfUse.IndexOf("other");
                if (index > -1)
                {
                    var TempString = carDetails.TermsOfUse.Substring(index + "other".Length);
                    ViewBag.SpecificOther = TempString.Substring(1);

                }
                else
                {
                    ViewBag.SpecificOther = "";
                }
            }

            if (carDetails == null)
            {
                return NotFound();
            }
			return View(carDetails);
        }

        [HttpGet]
        public async Task<IActionResult> RentCar(Guid carId)
        {
            var currentUser = await userManager.GetUserAsync(User);
            currentUser = accountRepository.GetInclude(x => x.Id == currentUser.Id, "Address").FirstOrDefault();

            var pickupDate = TempData["PickupDate"];
            var dropOffDate = TempData["DropOffDate"];
            string bookingNo = $"{DateTime.Now.ToString("yyyyMMdd")}{GenerateRandomNumericString(4)}";
            TempData["BookingNo"] = bookingNo;
            TempData["CarID"] = carId;
            double rentalDays = double.Parse(TempData["RentalDays"] as string);
            var carDetails = carRepository.GetInclude(c => c.CarId == carId, "BrandModel", "Color", "Address", "CarImage").SingleOrDefault();
            string pickupLocation = TempData["PickupLocation"] as string;

            if (carDetails == null)
            {
                return NotFound();
            }

            ViewBag.FrontImgRent = carDetails.CarImage.FrontImg;
            ViewBag.BackImgRent = carDetails.CarImage.BackImg;
            ViewBag.LeftImgRent = carDetails.CarImage.LeftImg;
            ViewBag.RightImgRent = carDetails.CarImage.RightImg;

            ViewBag.Email = currentUser.Email;
            ViewBag.PhoneNo = currentUser.PhoneNumber;
            ViewBag.Name = currentUser.FullName;
            ViewBag.DOB = currentUser.DateOfBirth;
            ViewBag.NationalID = currentUser.NationalID;
            ViewBag.Wallet = currentUser.Wallet;
            ViewBag.SpecificAddress = currentUser.SpecificAddress;

            ViewBag.Car = carDetails;
            ViewBag.BasePrice = carDetails.BasePrice;
            ViewBag.BookingNo = bookingNo;
            ViewBag.PickupDate = pickupDate;
            ViewBag.DropOffDate = dropOffDate;
            ViewBag.TotalDays = rentalDays;
            ViewBag.TotalPrice = (decimal)rentalDays * carDetails.BasePrice;
            ViewBag.Deposit = carDetails.Deposit;

            ViewBag.AddressCity = currentUser.Address.City;
            ViewBag.AddressDistrict = currentUser.Address.District;
            ViewBag.AddressWard = currentUser.Address.Ward;

            return View(carDetails);
        }

        [HttpPost]
        public async Task<IActionResult> RentCar(Booking booking, string PickUpDate, string DropOffDate, string BookingNo, string CarId)
        {
            var carOwner = accountRepository.GetAccountByCarId(Guid.Parse(CarId));

            var carDetails = carRepository.GetInclude(c => c.CarId == Guid.Parse(CarId), "BrandModel", "Color", "Address").SingleOrDefault();
            var currentUser = await userManager.GetUserAsync(User);
            string renterSelectedCity = Request.Form["RenterAddressCity"];
            string renterSelectedDistrict = Request.Form["RenterAddressDistrict"];
            string renterSelectedWard = Request.Form["RenterAddressWard"];
            string renterSpecificAddress = Request.Form["RenterSpecificAddress"];
            string driverSelectedCity = Request.Form["DriverAddressCity"];
            string driverSelectedDistrict = Request.Form["DriverAddressDistrict"];
            string driverSelectedWard = Request.Form["DriverAddressWard"];
            string driverSpecificAddress = Request.Form["DriverSpecificAddress"];

            var RenterAddress = addressRepository.GetInclude(x => x.Ward == renterSelectedWard
                                                          && x.District == renterSelectedDistrict
                                                          && x.City == renterSelectedCity).FirstOrDefault();
            var DriverAddress = addressRepository.GetInclude(x => x.Ward == driverSelectedWard
                                                          && x.District == driverSelectedDistrict
                                                          && x.City == driverSelectedCity).FirstOrDefault();
            if (carDetails.Deposit > currentUser.Wallet)
            {

            }
            var newBooking = new Booking()
            {
                BookingId = Guid.NewGuid(),
                BookingNo = BookingNo,
                StartDateTime = Convert.ToDateTime(PickUpDate),
                EndDateTime = Convert.ToDateTime(DropOffDate),
                PaymentMethod = "Wallet",
                NumberOfDays = (int)(Convert.ToDateTime(DropOffDate) - Convert.ToDateTime(PickUpDate)).TotalDays,
                BasePrice = carDetails.BasePrice,
                Total = (int)(Convert.ToDateTime(DropOffDate) - Convert.ToDateTime(PickUpDate)).TotalDays * carDetails.BasePrice,
                Deposit = carDetails.Deposit,
                Status = "1",
                DriverFullname = booking.DriverFullname,
                DriverDob = booking.DriverDob,
                DriverPhoneNumber = booking.DriverPhoneNumber,
                DriverEmail = booking.DriverEmail,
                DriverNationIdNumber = booking.DriverNationIdNumber,
                DriverDrivingLicense = "abc",
                DriverAddress = DriverAddress,
                DriverSpecificAddress = driverSpecificAddress,
                RenterFullname = booking.RenterFullname,
                RenterDob = booking.RenterDob,
                RenterPhoneNumber = booking.RenterPhoneNumber,
                RenterEmail = booking.RenterEmail,
                RenterNationIdNumber = booking.RenterNationIdNumber,
                RenterDrivingLicense = "abc",
                RenterAddress = RenterAddress,
                RenterSpecificAddress = renterSpecificAddress,
                UserId = currentUser.Id,
                CarId = carDetails.CarId
            };

            currentUser.Wallet -= carDetails.Deposit;
            var customerTransaction = new TransactionHistory
            {
                Id = Guid.NewGuid(),
                UserId = currentUser.Id.ToString(),
                BookingId = newBooking.BookingId,
                Amount = -carDetails.Deposit,
                TransactionDate = DateTime.Now,
                TransactionType = "Pay deposit",
                TransactionDescription = ""
            };

            carOwner.Wallet += carDetails.Deposit;
            var carOwnerTransaction = new TransactionHistory
            {
                Id = Guid.NewGuid(),
                UserId = carOwner.Id.ToString(),
                BookingId = newBooking.BookingId,
                Amount = carDetails.Deposit,
                TransactionDate = DateTime.Now,
                TransactionType = "Receive deposit",
                TransactionDescription = ""
            };

            transactionHistoryRepository.Add(customerTransaction);
            transactionHistoryRepository.Add(carOwnerTransaction);

            bookingRepository.Add(newBooking);
            var carUpdateStatus = carRepository.FindByID(newBooking.CarId);
            carUpdateStatus.CarStatus = Core.Enums.CarStatus.Booked;
            await carRepository.SaveChangeAsync();
            await userManager.UpdateAsync(currentUser);

            await bookingRepository.SaveChangeAsync();
            _toastNotification.AddSuccessToastMessage("Rent car successfully!! Please check your booking");
            return RedirectToAction("Index");
        }

        public JsonResult GetBrands()
        {
            var brands = brandModelRepository.GetBrands();
            return Json(brands);
        }

        public JsonResult GetModels(string selectedBrand)
        {
            var models = brandModelRepository.GetModelsByBrand(selectedBrand);

            return Json(models);
        }

        public JsonResult GetCities()
        {
            var cities = addressRepository.GetAll().Select(a => a.City).Distinct().ToList();

            return Json(cities);
        }

        public JsonResult GetDistricts(string selectedCity)
        {
            var districts = addressRepository.GetAll()
                .Where(a => a.City == selectedCity)
                .Select(a => a.District)
                .Distinct()
                .ToList();

            return Json(districts);
        }

        public JsonResult GetWards(string selectedDistrict)
        {
            var wards = addressRepository.GetAll()
                .Where(a => a.District == selectedDistrict)
                .Select(a => a.Ward)
                .Distinct()
                .ToList();

            return Json(wards);
        }

        public static string GenerateRandomNumericString(int length)
        {
            Random rand = new Random();
            StringBuilder result = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                int randomDigit = rand.Next(0, 10);
                result.Append(randomDigit);
            }
            return result.ToString();
        }
    }
}
