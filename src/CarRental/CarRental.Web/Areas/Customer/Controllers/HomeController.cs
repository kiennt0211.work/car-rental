﻿using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CarRental.Web.Areas.Customer.Controllers
{
    [Area("Customer")]
    [Authorize(Roles = ListRoles.Customer)]
    public class HomeController : Controller
    {
        private readonly ICarRepository carRepository;
        private readonly IFeedBackRepository _feedBackRepository;
        public HomeController(ICarRepository carRepository, IFeedBackRepository feedBackRepository)
        {
            this.carRepository = carRepository;
            _feedBackRepository = feedBackRepository;
        }
        public IActionResult Index()
        {
            var topCities = carRepository.GetTopCitiesWithMostCars();
            var feedBack = _feedBackRepository.GetInclude(x => true, "Booking");
            ViewBag.FeedBack = feedBack;
            return View(topCities);
        }

        public IActionResult AccessDenied() 
        {
            return View();
        }

    }
}
