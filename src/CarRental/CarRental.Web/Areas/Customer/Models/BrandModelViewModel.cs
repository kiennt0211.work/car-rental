﻿using CarRental.Core.Models;

namespace CarRental.Web.Areas.Customer.Models
{
    public class BrandModelViewModel
    {
        public int BrandModelId { get; set; }

        public string? Brand { get; set; }

        public string? Model { get; set; }
        public virtual ICollection<Car>? Cars { get; set; }
    }
}
