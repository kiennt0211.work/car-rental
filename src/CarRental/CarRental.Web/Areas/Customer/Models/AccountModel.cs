﻿using CarRental.Core.Models;
using System.ComponentModel.DataAnnotations;

namespace CarRental.Web.Areas.Customer.Models
{
    public class AccountModel
    {
        public string? FullName { get; set; }

        [Required(ErrorMessage = "Date of birth is required")]
        [DataType(DataType.Date, ErrorMessage = "Date of birth must be Date")]
        public DateTime DateOfBirth { get; set; } = DateTime.Now;
        public string? NationalID { get; set; }

        public string? DrivingLicense { get; set; }
        public decimal Wallet { get; set; } = 0;

        public int AddressID { get; set; } = 1;
        public Address? Address { get; set; }

        public virtual ICollection<Booking>? Bookings { get; set; }
        public virtual ICollection<Car>? Cars { get; set; }
    }
}
