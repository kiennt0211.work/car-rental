﻿using CarRental.Core.Models;
using System.ComponentModel.DataAnnotations;

namespace CarRental.Web.Areas.Customer.Models
{
    public class BookingModel
    {
		[Key]
		public Guid BookingId { get; set; }

		[Required(ErrorMessage = "Start Date is Required")]
		public DateTime StartDateTime { get; set; }

		[Required(ErrorMessage = "End Date is Required")]
		public DateTime EndDateTime { get; set; }

		[Required(ErrorMessage = "Payment Method is Required")]
		public string? PaymentMethod { get; set; }

		public string? BookingNo { get; set; }
		public int NumberOfDays { get; set; }
		public decimal BasePrice { get; set; }
		public decimal Total { get; set; }
		public decimal Deposit { get; set; }
		public string? Status { get; set; }

		public Guid UserId { get; set; }
		public Account? Account { get; set; }

		public virtual ICollection<Feedback>? Feedbacks { get; set; }

		public Guid? CarId { get; set; }
		public virtual Car? Car { get; set; }
		//Driver
		public string? RenterFullname { get; set; }

		public DateTime? RenterDob { get; set; }
		public string? RenterPhoneNumber { get; set; }
		public string? RenterEmail { get; set; }
		public string? RenterAddress { get; set; }
		public string? RenterDrivingLicense { get; set; }
		public string? RenterNationIdNumber { get; set; }

		public string? DriverFullname { get; set; }
		public DateTime? DriverDob { get; set; }
		public string? DriverPhoneNumber { get; set; }
		public string? DriverEmail { get; set; }
		public string? DriverAddress { get; set; }
		public string? DriverDrivingLicense { get; set; }
		public string? DriverNationIdNumber { get; set; }
	}
}
