﻿using CarRental.Core.Models;

namespace CarRental.Web.Areas.Customer.Models
{
    public class AddressModel
    {
        public int AddressId { get; set; }
        public string? Ward { get; set; }
        public string? District { get; set; }
        public string? City { get; set; }

        public virtual ICollection<Account>? Accounts { get; set; }
        public virtual ICollection<Car>? Cars { get; set; }
    }
}
