﻿using CarRental.Core.Models;

namespace CarRental.Web.Areas.Customer.Models
{
    public class 
        Model
    {
        public Guid BookingCarId { get; set; }
        public Guid BookingId { get; set; }
        public Booking? Booking { get; set; }

        public Guid CarId { get; set; }
        public Car? Car { get; set; }
    }
}
