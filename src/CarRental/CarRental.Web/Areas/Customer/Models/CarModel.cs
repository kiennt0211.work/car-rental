﻿using CarRental.Core.Models;
using System.ComponentModel.DataAnnotations;

namespace CarRental.Web.Areas.Customer.Models
{
    public class CarModel
    {
        [Key]
        public Guid CarId { get; set; }
        [Required(ErrorMessage = "Car Name is Required!")]
        public string CarName { get; set; } = null!;

        [Required(ErrorMessage = "License Plate is Required!")]
        public string LicensePlate { get; set; } = null!;
        public int? BrandModelId { get; set; }
        public BrandModel? BrandModel { get; set; }
        public int ColorId { get; set; }
        public Color? Color { get; set; }

        public int NumberOfSeats { get; set; } = 0;

        [Required(ErrorMessage = "Model Name is Required!")]
        [Range(1000, 9999, ErrorMessage = "Invalid Production Year")]
        public int ProductionYears { get; set; }

        [Required(ErrorMessage = "Transmission Type is Required!")]
        public string TransmissionType { get; set; } = null!;

        [Required(ErrorMessage = "Fuel Type is Required!")]
        public string FuelType { get; set; } = null!;
        public decimal Mileage { get; set; } = 0;
        public decimal FuelConsumption { get; set; } = 0;
        public decimal BasePrice { get; set; } = 0;
        public decimal Deposit { get; set; } = 0;
        public string? Description { get; set; }
        public string? AdditionalFunctions { get; set; }
        public string? TermsOfUse { get; set; }

        public int? AddressID { get; set; }
        public Address? Address { get; set; }

        public Guid UserID { get; set; }
        public Account? AppUser { get; set; }

        public Guid? CarImgId { get; set; }

        public virtual CarImage? CarImage { get; set; }
    }
}
