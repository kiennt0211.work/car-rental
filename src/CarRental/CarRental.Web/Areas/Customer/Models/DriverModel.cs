﻿using CarRental.Core.Models;
using System.ComponentModel.DataAnnotations;

namespace CarRental.Web.Areas.Customer.Models
{
    public class DriverModel
    {
        public Guid DriverId { get; set; }

        [Required(ErrorMessage = "User Name is required")]
        public string FullName { get; set; } = null!;

        [Required(ErrorMessage = "Date of birth is required")]
        [DataType(DataType.Date, ErrorMessage = "Date of birth must be Date")]
        public DateTime DateOfBirth { get; set; }

        [Required(ErrorMessage = "National ID is required")]
        public string NationalID { get; set; } = null!;

        [Required(ErrorMessage = "Phone Number is required")]
        [Phone(ErrorMessage = "Invalid Phone number!")]
        public string PhoneNumber { get; set; } = null!;

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Invalid Email")]
        public string Email { get; set; } = null!;

        [Required(ErrorMessage = "Driving License is required")]
        public string DrivingLicense { get; set; } = null!;
        public Guid AddressId { get; set; }
        public Address? Address { get; set; }
    }
}
