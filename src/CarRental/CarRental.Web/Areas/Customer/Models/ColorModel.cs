﻿using MessagePack;
using System.ComponentModel.DataAnnotations;

namespace CarRental.Web.Areas.Customer.Models
{
    public class ColorModel
    {
        public int ColorId { get; set; }

        [Required(ErrorMessage = "Color Name is Required")]
        public string ColorName { get; set; } = null!;
    }
}
