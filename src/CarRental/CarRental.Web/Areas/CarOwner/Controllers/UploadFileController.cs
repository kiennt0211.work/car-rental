﻿using Microsoft.AspNetCore.Mvc;

namespace YourNamespace.Controllers
{
    [Area("CarOwner")]
    [Route("CarOwner/UploadFile")]
    public class UploadFileController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public UploadFileController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpPost]
        [Route("UploadImage")]
        public async Task<IActionResult> UploadImage(IFormFile fileUpload)
        {
            try
            {
                if (fileUpload != null && fileUpload.Length > 0)
                {
                    string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "tempImgFile");

                    // Create folder where save temporary img if tempImgFile is not exists
                    Directory.CreateDirectory(uploadsFolder);

                    // Create id for each image imported
                    string uniqueFileName = Guid.NewGuid().ToString() + "_" + fileUpload.FileName;

                    string filePath = Path.Combine(uploadsFolder, uniqueFileName);

                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await fileUpload.CopyToAsync(stream);
                    }

                    string imageUrl = "/tempImgFile/" + uniqueFileName;
                    return Ok(imageUrl);
                }
                else
                {
                    return BadRequest("No file uploaded.");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error: " + ex.Message);
            }
        }
    }
}
