﻿using AutoMapper;
using CarRental.Core.Data;
using CarRental.Core.Models;
using CarRental.DataAccess.Repositories.Interfaces;
using CarRental.Web.Areas.CarOwner.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NToastNotify;
using X.PagedList;

namespace CarRental.Web.Areas.CarOwner.Controllers
{
    [Area("CarOwner")]
    [Authorize(Roles = ListRoles.CarOwner)]
    public class CarsController : Controller
    {
        private readonly ICarRepository carRepository;
        private readonly IBrandModelRepository brandModelRepository;
        private readonly IAddressRepository addressRepository;
        private readonly IBaseRepository<Color> colorRepository;
        private readonly IBookingRepository bookingRepository;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment webHost;
        private readonly IToastNotification _toastNotification;
        private readonly UserManager<Account> userManager;
        private readonly CarRentalContext _context;

        public CarsController(ICarRepository carRepository,
            IBrandModelRepository brandModelRepository,
            IAddressRepository addressRepository,
            IBaseRepository<Color> colorRepository,
            IBookingRepository bookingRepository,
            IMapper mapper,
            IWebHostEnvironment webHost,
            IToastNotification toastNotification,
            UserManager<Account> userManager,
            CarRentalContext context)
        {
            this.carRepository = carRepository;
            this.brandModelRepository = brandModelRepository;
            this.addressRepository = addressRepository;
            this.colorRepository = colorRepository;
            this.bookingRepository = bookingRepository;
            _mapper = mapper;
            this.webHost = webHost;
            this._toastNotification = toastNotification;
            this.userManager = userManager;
            _context = context;
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.ColorId = new SelectList(colorRepository.GetAll().ToList(), "ColorId", "ColorName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AddCarViewModel carModel)
        {
            var user = await userManager.GetUserAsync(User);
            if (user != null)
            {
                carModel.UserID = user.Id;
            }
            if (ModelState.IsValid)
            {
                Car newCar = new Car();
                newCar = _mapper.Map<Car>(carModel);

                var address = addressRepository.GetAll()
                            .FirstOrDefault(x =>
                            x.City == carModel.Address.City
                            && x.District == carModel.Address.District
                            && x.Ward == carModel.Address.Ward);


                var brandModel = brandModelRepository.GetAll()
                    .FirstOrDefault(x =>
                    x.Brand == carModel.BrandModel.Brand
                    && x.Model == carModel.BrandModel.Model);

                if (address == null || brandModel == null)
                {
                    return NotFound();
                }

                if (newCar.AdditionalFunctions != null)
                {
                    newCar.AdditionalFunctions = string.Join(",", carModel.AdditionalFunctions);

                }
                if (newCar.TermsOfUse != null)
                {
                    newCar.TermsOfUse = string.Join(",", carModel.TermsOfUse);
                }

                newCar.CarId = Guid.NewGuid();
                newCar.CarName = string.Format($"{newCar.BrandModel.Brand} {newCar.BrandModel.Model} {newCar.ProductionYears}");
                newCar.Address = address;
                newCar.BrandModel = brandModel;
                newCar.AddressID = address.AddressId;
                newCar.BrandModelId = brandModel.BrandModelId;

                CarImage CarImage = new CarImage()
                {
                    Id = Guid.NewGuid(),
                    Car = newCar,
                    CarId = newCar.CarId,
                    CertificateImg = UploadCarImage(carModel.CertificateImg),
                    InsuranceImg = UploadCarImage(carModel.InsuranceImg),
                    Registration = UploadCarImage(carModel.RegistrationImg),
                    FrontImg = UploadCarImage(carModel.FrontImg),
                    BackImg = UploadCarImage(carModel.BackImg),
                    LeftImg = UploadCarImage(carModel.LeftImg),
                    RightImg = UploadCarImage(carModel.RightImg)
                };

                newCar.CarImgId = CarImage.Id;
                newCar.CarImage = CarImage;

                newCar.SpecificAddress = carModel.SpecificAddress;

                carRepository.Add(newCar);
                await carRepository.SaveChangeAsync();

                // xoa folder tam thoi luu anh
                var tempFileFolder = Path.Combine(webHost.WebRootPath, "tempImgFile");
                if (Directory.Exists(tempFileFolder))
                {
                    Directory.Delete(tempFileFolder, true);
                }
                _toastNotification.AddSuccessToastMessage("Add new car successfully!! Please check your car");

                return RedirectToAction(nameof(ViewCarsList));
            }

            return View(carModel);
        }

        public async Task<IActionResult> ViewCarsList()
        {
            var user = await userManager.GetUserAsync(User);
            var cars = await carRepository.GetInclude(x => x.UserID == user.Id, "Address", "Color", "BrandModel", "CarImage")
                .ToListAsync();

            var ratings = _context.Cars.Include(x => x.Bookings).ThenInclude(x => x.Feedbacks).FirstOrDefault();

            if (ratings != null)
            {
                int ratingSum = (int)ratings.Bookings
                    .SelectMany(booking => booking.Feedbacks)
                    .Sum(feedback => feedback.Ratings);

                int countRatings = ratings.Bookings
                    .SelectMany(booking => booking.Feedbacks)
                    .Count();

                decimal avgRate = countRatings > 0 ? (decimal)ratingSum / countRatings : 0;

                ViewBag.TotalRatings = ratingSum;
                ViewBag.RateCount = countRatings;
                ViewBag.AvgRate = (double)avgRate;
            }
            return View(cars);
        }

        [HttpGet]
        public async Task<IActionResult> SortAndPaginateCars(string sortOrder, int pageNumber = 1, int pageSize = 3)
        {
            var currentUser = await userManager.GetUserAsync(User);
            var query = carRepository.GetInclude(x => x.UserID == currentUser.Id, "Address", "BrandModel", "CarImage");

            IEnumerable<Car> cars;

            switch (sortOrder)
            {
                case "Newest to Lastest":
                    cars = await query.OrderByDescending(x => x.CreateAt).ToListAsync();
                    break;
                case "Lastest to Newest":
                    cars = await query.OrderBy(x => x.ProductionYears).ToListAsync();
                    break;
                case "Order By Price":
                    cars = await query.OrderBy(x => x.BasePrice).ToListAsync();
                    break;
                default:
                    // Xử lý lựa chọn sắp xếp không hợp lệ ở đây
                    return BadRequest("Invalid sort option");
            }
            var totalCount = cars.Count();
            var totalPages = (int)Math.Ceiling((double)totalCount / pageSize);
            ViewBag.TotalPages = totalPages;

            var paginatedCars = cars.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();

            return PartialView("_CarsListPartial", paginatedCars);
        }



        /*        public IActionResult List()
                {
                    var cars = carRepository.GetAll();
                    return View(cars);
                }*/

        [HttpGet]
        public IActionResult EditCar(Guid carId)
        {
            var car = carRepository
                .GetInclude(x => x.CarId == carId, "Address", "CarImage", "Color", "BrandModel")
                .FirstOrDefault();

            EditCarViewModel CarModel = _mapper.Map<EditCarViewModel>(car);
            string[] listAddtional;
            string[] listTermOfUse;
            if (car.AdditionalFunctions != null && car.AdditionalFunctions.Count() > 0)
            {
                listAddtional = car.AdditionalFunctions.Split(",");
            }
            else
            {
                listAddtional = new string[1] { "" };
            }

            if (car.TermsOfUse != null && car.TermsOfUse.Count() > 0)
            {
                listTermOfUse = car.TermsOfUse.Split(",");
            }
            else
            {
                listTermOfUse = new string[1] { "" };
            }


            CarModel.AdditionalFunctionsEditCar = new List<string>(listAddtional);
            CarModel.TermsOfUseEditCar = new List<string>(listTermOfUse);

            if (CarModel.TermsOfUseEditCar.Any(x => x.Equals("other", StringComparison.CurrentCultureIgnoreCase)))
            {
                var listOtherTermOfUse = CarModel.TermsOfUseEditCar.SkipWhile(x => x != "other").Skip(1).ToList();
                CarModel.OtherTermOfUse = string.Join(",", listOtherTermOfUse);
            }


            var ratings = _context.Cars.Include(x => x.Bookings).ThenInclude(x => x.Feedbacks).FirstOrDefault(x => x.CarId == carId);

            if (ratings != null)
            {
                int ratingSum = (int)ratings.Bookings
                    .SelectMany(booking => booking.Feedbacks)
                    .Sum(feedback => feedback.Ratings);

                int countRatings = ratings.Bookings
                    .SelectMany(booking => booking.Feedbacks)
                    .Count();

                decimal avgRate = countRatings > 0 ? (decimal)ratingSum / countRatings : 0;

                ViewBag.TotalRatings = ratingSum;
                ViewBag.RateCount = countRatings;
                ViewBag.AvgRate = (double)avgRate;
            }

            return View(CarModel);
        }

        [HttpPost]
        public async Task<IActionResult> EditCar(Guid CarId, EditCarViewModel carModel)
        {
            var address = addressRepository.GetInclude(x => x.Ward == carModel.Address.Ward
            && x.District == carModel.Address.District
            && x.City == carModel.Address.City).FirstOrDefault();

            var carEdit = carRepository.GetInclude(x => x.CarId == CarId, "Address", "CarImage").FirstOrDefault();
            try
            {

                carEdit.CarStatus = carModel.CarStatus;
                carEdit.Mileage = carModel.Mileage;
                carEdit.FuelConsumption = carModel.FuelConsumption;

                carEdit.AddressID = address.AddressId;

                carEdit.SpecificAddress = carModel.SpecificAddress;

                carEdit.Description = carModel.Description;
                if (!string.IsNullOrEmpty(UploadCarImage(carModel.FrontImg)))
                {
                    carEdit.CarImage.FrontImg = UploadCarImage(carModel.FrontImg);
                }
                if (!string.IsNullOrEmpty(UploadCarImage(carModel.BackImg)))
                {
                    carEdit.CarImage.BackImg = UploadCarImage(carModel.BackImg);
                }
                if (!string.IsNullOrEmpty(UploadCarImage(carModel.LeftImg)))
                {
                    carEdit.CarImage.LeftImg = UploadCarImage(carModel.LeftImg);
                }
                if (!string.IsNullOrEmpty(UploadCarImage(carModel.RightImg)))
                {
                    carEdit.CarImage.RightImg = UploadCarImage(carModel.RightImg);
                }

                if (carModel.AdditionalFunctionsEditCar != null && carModel.AdditionalFunctionsEditCar.Count() > 0)
                {
                    carEdit.AdditionalFunctions = string.Join(",", carModel.AdditionalFunctionsEditCar);
                }
                else
                {
                    carEdit.AdditionalFunctions = string.Empty;
                }

                if (carModel.TermsOfUseEditCar != null && carModel.TermsOfUseEditCar.Count() > 0)
                {
                    carEdit.TermsOfUse = string.Join(",", carModel.TermsOfUseEditCar);
                    if (carModel.TermsOfUseEditCar.Any(x => x.Equals("other", StringComparison.CurrentCultureIgnoreCase)))
                    {
                        carEdit.TermsOfUse += string.Format($", {carModel.OtherTermOfUse}");
                    }
                }
                else
                {
                    carEdit.TermsOfUse = string.Empty;
                }


                carEdit.BasePrice = carModel.BasePrice;
                carEdit.Deposit = carModel.Deposit;

                carRepository.Update(carEdit);

                await carRepository.SaveChangeAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (carRepository.FindByID(carModel.CarId) == null)
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            var tempFileFolder = Path.Combine(webHost.WebRootPath, "tempImgFile");
            if (Directory.Exists(tempFileFolder))
            {
                Directory.Delete(tempFileFolder, true);
            }
            _toastNotification.AddSuccessToastMessage("Edit car successfully!! Please check your car");
            return RedirectToAction(nameof(ViewCarsList));
        }

        public JsonResult GetBrands()
        {
            var brands = brandModelRepository.GetAll().Select(b => b.Brand).Distinct().ToList();
            return Json(brands);
        }

        public JsonResult GetModels(string selectedBrand)
        {
            var models = brandModelRepository.GetAll().Where(b => b.Brand == selectedBrand).Select(b => b.Model).ToList();

            return Json(models);
        }

        public int GetBrandModelId(string selectedModel, string selectedBrand)
        {
            int brandModelId = brandModelRepository.GetInclude(m => m.Model == selectedModel && m.Brand == selectedBrand).SingleOrDefault()?.BrandModelId ?? 0;

            return brandModelId;
        }

        public JsonResult GetCities()
        {
            var cities = addressRepository.GetAll().Select(a => a.City).Distinct().ToList();

            return Json(cities);
        }

        public JsonResult GetDistricts(string selectedCity)
        {
            var districts = addressRepository.GetAll()
                .Where(a => a.City == selectedCity)
                .Select(a => a.District)
                .Distinct()
                .ToList();

            return Json(districts);
        }

        public JsonResult GetWards(string selectedDistrict)
        {
            var wards = addressRepository.GetAll()
                .Where(a => a.District == selectedDistrict)
                .Select(a => a.Ward)
                .Distinct()
                .ToList();

            return Json(wards);
        }

        public int GetAddressId(string selectedWard, string selectedDistrict, string selectedCity)
        {
            int brandModelId = addressRepository.GetInclude(m => m.Ward == selectedWard && m.District == selectedDistrict && m.City == selectedCity).SingleOrDefault()?.AddressId ?? 0;

            return brandModelId;
        }

        public JsonResult GetColors()
        {
            var colors = colorRepository.GetAll()
                .Select(c => new {
                    ColorId = c.ColorId,
                    ColorName = c.ColorName
                }).Distinct().ToList();
            return Json(colors);
        }

        private string UploadCarImage(IFormFile? file)
        {
            string url = "";

            if (file != null)
            {
                var root = webHost.WebRootPath;
                var carImagesPath = "upload-car-images";
                var prefix = Guid.NewGuid().ToString().ToUpper().Split('-')[0];
                var uploadFolder = Path.Combine(root, carImagesPath);

                Directory.CreateDirectory(uploadFolder);

                var filePath = Path.Combine(root, carImagesPath, $"{prefix}-{file.FileName}");
                using (var stream = System.IO.File.Create(filePath))
                {
                    file.CopyTo(stream);
                }

                url = $"/{carImagesPath}/{prefix}-{file.FileName}";
                return url;
            }

            return string.Empty;
        }

        public async Task<IActionResult> Report(string filter = "All")
        {
            var currentUser = await userManager.GetUserAsync(User);
            var feedbacks = await carRepository.GetFeedbacksByStar(filter, currentUser);

            var reports = _mapper.Map<List<FeedBackViewModel>>(feedbacks);
            for (int i = 0; i < feedbacks.Count; i++)
            {
                reports[i].CarImage = feedbacks[i].Booking.Car.CarImage.RightImg;
            }
            if (reports.Count != 0)
            {
                var averageRating = reports.Select(x => x.Ratings).Average();
                ViewBag.Stars = ((int)averageRating);
            }
            ViewBag.Filter = filter;
            ViewBag.AverageRating = carRepository.GetAverageStarForUser(currentUser).ToString("#,#");

            return View(reports);
        }

    }
}
