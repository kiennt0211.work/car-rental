﻿using CarRental.Core.Models;
using System.ComponentModel.DataAnnotations;

namespace CarRental.Web.Areas.CarOwner.ViewModels
{
    public class AddCarViewModel
    {
        [Key]
        public Guid CarId { get; set; }
        public string? CarName { get; set; }

        public string? LicensePlate { get; set; }
        public int? BrandModelId { get; set; }
        public BrandModel? BrandModel { get; set; }
        public int ColorId { get; set; }
        public Color? Color { get; set; }

        public int NumberOfSeats { get; set; } = 0;

        [Required(ErrorMessage = "Model Name is Required!")]
        [Range(1000, 9999, ErrorMessage = "Invalid Production Year")]
        public int ProductionYears { get; set; }

        [Required(ErrorMessage = "Transmission Type is Required!")]
        public string TransmissionType { get; set; } = null!;

		public string? SpecificAddress { get; set; }
		[Required(ErrorMessage = "Fuel Type is Required!")]
        public string FuelType { get; set; } = null!;
        public decimal Mileage { get; set; } = 0;
        public decimal FuelConsumption { get; set; } = 0;
        public decimal BasePrice { get; set; } = 0;
        public decimal Deposit { get; set; } = 0;
        public string? Description { get; set; }
        public List<string>? AdditionalFunctions { get; set; }
        public List<string>? TermsOfUse { get; set; }
        public string? Images { get; set; }
        
        public IFormFile? FrontImg { get; set; }
        public IFormFile? BackImg { get; set; }
        public IFormFile? LeftImg { get; set; }
        public IFormFile? RightImg { get; set; }
        public IFormFile? CertificateImg { get; set; }
        public IFormFile? RegistrationImg { get; set; }
        public IFormFile? InsuranceImg { get; set; }    
        public int? AddressID { get; set; }
        public Address? Address { get; set; }

        public Guid? UserID { get; set; }
        public Account? AppUser { get; set; }
    }
}
