﻿using CarRental.Core.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CarRental.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = ListRoles.Admin)]
    public class HomeController : Controller
	{
        private readonly UserManager<Account> userManager;
        private readonly SignInManager<Account> signInManager;

        public HomeController(UserManager<Account> userManager,
								SignInManager<Account> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }
        public IActionResult Index()
		{  
			return View();
		}

        public async Task<IActionResult> LogOut()
        {
            await this.signInManager.SignOutAsync();
            return Redirect(Url.Content("~/Accounts/Account/Login"));
        }
	}
}
