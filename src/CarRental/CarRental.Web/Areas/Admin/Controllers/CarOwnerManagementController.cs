﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CarRental.Core.Models;
using Microsoft.AspNetCore.Identity;
using CarRental.DataAccess.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace CarRental.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = ListRoles.Admin)]
    public class CarOwnerManagementController : Controller
    {
        private readonly UserManager<Account> userManager;
        private readonly SignInManager<Account> signInManager;
        private readonly IAccountRepository accountRepository;
        private readonly IAddressRepository addressRepository;

        public CarOwnerManagementController(
            UserManager<Account> userManager,
            SignInManager<Account> signInManager,
            IAccountRepository accountRepository,
            IAddressRepository addressRepository)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.accountRepository = accountRepository;
            this.addressRepository = addressRepository;
        }

        // GET: Admin/CarOwnerManagement
        public async Task<IActionResult> Index()
        {
            var carRentalContext = await userManager.GetUsersInRoleAsync(ListRoles.CarOwner);
            return View(carRentalContext);
        }

        // GET: Admin/CarOwnerManagement/Edit/5
        public IActionResult Edit(Guid id)
        {
            
            if (accountRepository.GetAll() == null)
            {
                return NotFound();
            }
            var result = accountRepository.FindByID(id);
            if (result == null)
            {
                return NotFound();
            }
            ViewData["AddressID"] = new SelectList(addressRepository.GetAll(), "AddressId", "AddressId", result.AddressID);
            return View(result);
        }
        [HttpPost]
        public async Task<IActionResult> EditStatus(Guid id, [Bind("Id, IsActive")] Account account)
        {
            if (id != account.Id)
            {
                return NotFound();
            }
                try
                {
                var result = accountRepository.FindByID(id);
                result.IsActive = !result.IsActive;
                    accountRepository.Update(result);
                await accountRepository.SaveChangeAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (accountRepository.FindByID(id) == null)
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> Delete(Account account)
        {
            var result = accountRepository.FindByID(account.Id);
            accountRepository.Delete(result);
            await accountRepository.SaveChangeAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
