﻿namespace CarRental.Web.Models
{
    public class EmailTemplate
    {
        public static string ForgotPasswordEmail(string url, string name = "")
        {
            name = name ?? string.Empty;

            var result = $@"
                Hi {name},

                There was a request to change your password!

                If you did not make this request then please ignore this email.

                Otherwise, please click this link to change your password: {url}";
            return result;
        }
    }
}
