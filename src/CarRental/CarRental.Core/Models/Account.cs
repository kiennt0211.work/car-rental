﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace CarRental.Core.Models
{
    public class Account : IdentityUser<Guid>
    {
        public string? FullName { get; set; }

        [Required(ErrorMessage = "Date of birth is required")]
        [DataType(DataType.Date, ErrorMessage = "Date of birth must be Date")]
        public DateTime DateOfBirth { get; set; } = DateTime.Now;
        public string? NationalID { get; set; }

        public string? DrivingLicense { get; set; }
        public decimal Wallet { get; set; } = 0;

        public int? AddressID { get; set; }
        public Address? Address { get; set; }

        public string? SpecificAddress {  get; set; }

        public bool IsActive { get; set; } = true;
        public virtual ICollection<Booking>? Bookings { get; set; }
        public virtual ICollection<Car>? Cars { get; set; }
        public virtual ICollection<TransactionHistory>? TransactionHistories { get; set; }
    }
}
