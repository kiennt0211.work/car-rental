﻿using System.ComponentModel.DataAnnotations;

namespace CarRental.Core.Models
{
	public class Booking
    {
        [Key]
        public Guid BookingId { get; set; }

        [Required(ErrorMessage = "Start Date is Required")]
        public DateTime StartDateTime { get; set; }

        [Required(ErrorMessage = "End Date is Required")]
        public DateTime EndDateTime { get; set; }

        [Required(ErrorMessage = "Payment Method is Required")]
        public string? PaymentMethod { get; set; }

        public string? BookingNo { get; set; }
        public int NumberOfDays { get; set; }
        public decimal BasePrice { get; set; }
        public decimal Total { get; set; }
        public decimal Deposit { get; set; }
        public string? Status { get; set; }

        public Guid UserId { get; set; }
        public Account? Account { get; set; }

        public virtual ICollection<Feedback> Feedbacks { get; set; }

        public Guid? CarId {  get; set; }
        public virtual Car? Car { get; set; }
        //Driver

        [Required(ErrorMessage = "Renter full name is Required")]
        public string? RenterFullname { get; set; }
        [Required(ErrorMessage = "Renter Date of Birth is Required")]
        public DateTime? RenterDob { get; set; }
        [Required(ErrorMessage = "Renter phone number is Required")]
        public string? RenterPhoneNumber { get; set; }
        [Required(ErrorMessage = "Renter email is Required")]
        public string? RenterEmail { get; set; }
        public string? RenterSpecificAddress { get; set; }
        public Address? RenterAddress { get; set; }
        [Required(ErrorMessage = "Renter driving license is Required")]
        public string? RenterDrivingLicense { get; set; }
        [Required(ErrorMessage = "Renter nation id number is Required")]
        public string? RenterNationIdNumber { get; set; }
        [Required(ErrorMessage = "Driver full name is Required")]
        public string? DriverFullname { get; set; }
        [Required(ErrorMessage = "Driver Date of Birth is Required")]
        public DateTime? DriverDob { get; set; }
        [Required(ErrorMessage = "Driver phone number is Required")]
        public string? DriverPhoneNumber { get; set; }
        [Required(ErrorMessage = "Driver email is Required")]
        public string? DriverEmail { get; set; }

        public string? DriverSpecificAddress { get; set; }
        public Address? DriverAddress { get; set; }

        [Required(ErrorMessage = "Driver driving license is Required")]
        public string? DriverDrivingLicense { get; set; }
        [Required(ErrorMessage = "Driver nation id number is Required")]
        public string? DriverNationIdNumber { get; set; }
    }
}
