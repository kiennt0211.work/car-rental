﻿using System.ComponentModel.DataAnnotations;

namespace CarRental.Core.Models
{
    public class Color
    {
        [Key]
        public int ColorId { get; set; }

        [Required(ErrorMessage = "Color Name is Required")]
        public string ColorName { get; set; } = null!;

        public virtual ICollection<Car>? Cars { get; set; }
    }
}
