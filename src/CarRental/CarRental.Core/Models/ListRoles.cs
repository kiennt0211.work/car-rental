﻿namespace CarRental.Core.Models
{
    public class ListRoles
    {
        public const string Customer = "Customer";
        public const string CarOwner = "CarOwner";
        public const string Admin = "Admin";
    }
}
