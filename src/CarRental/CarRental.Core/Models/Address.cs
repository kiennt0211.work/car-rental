﻿using System.ComponentModel.DataAnnotations;

namespace CarRental.Core.Models
{
    public class Address
    {
        [Key]
        public int? AddressId { get; set; }
        public string? WardCode { get; set; }
        public string? Ward { get; set; }
        public string? DistrictCode { get; set; }
        public string? District { get; set; }
        public string? CityCode { get; set; }
        public string? City { get; set; }

        public virtual ICollection<Account>? Accounts { get; set; }
        public virtual ICollection<Car>? Cars { get; set; }
    }
}
