﻿using System.ComponentModel.DataAnnotations;

namespace CarRental.Core.Models
{
	public class BrandModel
	{
		[Key]
        public int BrandModelId { get; set; }

		public string? Brand { get; set; }

        public string? Model { get; set; }
        public virtual ICollection<Car>? Cars { get; set; }
    }
}
