﻿using System.ComponentModel.DataAnnotations;

namespace CarRental.Core.Models
{
    public class CarImage
    {
        [Key]
        public Guid Id { get; set; }
        public string? CertificateImg { get; set; }
        public string? Registration { get; set; }
        public string? InsuranceImg { get; set; }
        public string? FrontImg { get; set; }
        public string? BackImg { get; set; }
        public string? LeftImg { get; set; }
        public string? RightImg { get; set; }

        public Guid? CarId { get; set; }
        public virtual Car? Car { get; set; }
    }
}
