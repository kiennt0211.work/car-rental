﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRental.Core.Models
{
	public class TransactionHistory
	{
		[Key]
		public Guid Id { get; set; }
        public string? UserId { get; set; }
        public virtual Account? User { get; set; }
        public Guid? BookingId { get; set; }
        public virtual Booking? Booking { get; set; }

        [Precision(18, 2)]
        public decimal? Amount { get; set; }

        public DateTime? TransactionDate { get; set; } = DateTime.Now;

        [StringLength(255)]
        public string? TransactionType { get; set; }

        [StringLength(255)]
        public string? TransactionDescription { get; set; }

    }
}
