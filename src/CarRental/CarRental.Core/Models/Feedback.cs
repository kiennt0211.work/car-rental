﻿using System.ComponentModel.DataAnnotations;

namespace CarRental.Core.Models
{
    public class Feedback
    {
        [Key]
        public Guid FeedbackId { get; set; }

        public decimal Ratings { get; set; } = 0;

        [Required(ErrorMessage = "Feedback Content is required!")]
        public string Content { get; set; } = null!;
        public DateTime FeedbackDate { get; set; } = DateTime.Now;

        public Guid BookingId { get; set; }
        public Booking? Booking { get; set; }
    }
}
