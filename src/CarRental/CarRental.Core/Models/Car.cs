﻿using CarRental.Core.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRental.Core.Models
{
    public class Car
    {
        [Key]
        public Guid CarId { get; set; }
        public string? CarName { get; set; }

        [Required(ErrorMessage = "License Plate is Required!")]
        public string LicensePlate { get; set; } = null!;
        public int? BrandModelId { get; set; }
        public BrandModel? BrandModel { get; set; }
        public int ColorId { get; set; }
        public Color? Color { get; set; }

        public int NumberOfSeats { get; set; } = 0;

        [Range(1000, 9999, ErrorMessage = "Invalid Production Year")]
        public int ProductionYears { get; set; }
        public string TransmissionType { get; set; } = null!;

        public string? FuelType { get; set; } = null!;
        public decimal Mileage { get; set; } = 0;
        public decimal FuelConsumption { get; set; } = 0;
        public decimal BasePrice { get; set; } = 0;
        public decimal Deposit { get; set; } = 0;
        public string? Description { get; set; }
        public string? AdditionalFunctions { get; set; }
        public string? TermsOfUse { get; set; }
        public string? SpecificAddress { get; set; }
        public CarStatus CarStatus { get; set; } = CarStatus.Available;
        public int? AddressID { get; set; }
        public Address? Address { get; set; }

        public Guid UserID { get; set; }
        public Account? AppUser { get; set; }

        public Guid? CarImgId { get; set; }

        public DateTime? CreateAt { get; set; } = DateTime.Now;
        public virtual CarImage? CarImage { get; set; }

        public virtual ICollection<Booking>? Bookings { get; set; }
    }
}