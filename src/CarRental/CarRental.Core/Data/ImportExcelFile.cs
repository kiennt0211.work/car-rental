﻿using CarRental.Core.Models;
using ClosedXML.Excel;

namespace CarRental.Core.Data
{
    public static class ImportExcelFile
    {
        public static List<Address> ImportAddresses(string path)
        {
            var workbook = new XLWorkbook(path);
            var worksheet = workbook.Worksheet(1);
            var i = 1;

            var addressesDataList = new List<Address>();

            foreach (var row in worksheet.RowsUsed().Skip(1)) // Skip header row
            {
                var addressData = new Address()
                {
                    AddressId = i,
                    WardCode = row.Cell(1).Value.ToString(),
                    Ward = row.Cell(2).Value.ToString(),
                    DistrictCode = row.Cell(3).Value.ToString(),
                    District = row.Cell(4).Value.ToString(),
                    CityCode = row.Cell(5).Value.ToString(),
                    City = row.Cell(6).Value.ToString()
                };
                i++;
                addressesDataList.Add(addressData);
            }
            return addressesDataList;
        }

        public static List<BrandModel> ImportBrandModels(string path)
        {
            var workbook = new XLWorkbook(path);
            var worksheet = workbook.Worksheet(1);
            var i = 1;

            var brandModelsDataList = new List<BrandModel>();

            foreach (var row in worksheet.RowsUsed().Skip(1)) // Skip header row
            {
                var brandModelData = new BrandModel()
                {
                    BrandModelId = i,
                    Brand = row.Cell(2).Value.ToString(),
                    Model = row.Cell(3).Value.ToString(),
                };
                i++;
                brandModelsDataList.Add(brandModelData);
            }

            return brandModelsDataList;
        }

        public static List<Color> ImportColor(string path)
        {
            var workbook = new XLWorkbook(path);
            var worksheet = workbook.Worksheet(1);
            var i = 1;

            var colorDataList = new List<Color>();

            foreach (var row in worksheet.RowsUsed().Skip(1)) // Skip header row
            {
                var data = new Color()
                {
                    ColorId = i,
                    ColorName = row.Cell(2).Value.ToString(),
                };
                i++;
                colorDataList.Add(data);
            }

            return colorDataList;
        }
    }
}
