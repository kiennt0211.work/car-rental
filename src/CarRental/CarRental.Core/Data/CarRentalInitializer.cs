﻿using CarRental.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace CarRental.Core.Data
{
    public partial class CarRentalContext
    {
        private void SeedingData(ModelBuilder modelBuilder)
        {
            List<IdentityRole<Guid>> Roles = new List<IdentityRole<Guid>>
            {
                new IdentityRole<Guid>()
                {
                    Id = Guid.NewGuid(),
                    Name = ListRoles.Customer,
                    ConcurrencyStamp = DateTime.Now.ToString(),
                    NormalizedName = ListRoles.Customer.ToUpper()
                },
                new IdentityRole<Guid>()
                {
                    Id = Guid.NewGuid(),
                    Name = ListRoles.CarOwner,
                    ConcurrencyStamp = DateTime.Now.ToString(),
                    NormalizedName = ListRoles.CarOwner.ToUpper()
                },
                new IdentityRole<Guid>()
                {
                    Id = Guid.NewGuid(),
                    Name = ListRoles.Admin,
                    ConcurrencyStamp = DateTime.Now.ToString(),
                    NormalizedName = ListRoles.Admin.ToUpper()
                },
            };

            var passwordHasher = new PasswordHasher<Account>();

            List<Account> accounts = new List<Account>
            {
                new Account()
                {
                    Id = new Guid("91BE98E7-45A1-44F3-A5A8-A1892CD140F0"),
                    UserName = "user1@email.com",
                    Email = "user1@email.com",
                    NormalizedEmail = "USER1@EMAIL.COM",
                    NormalizedUserName = "USER1@EMAIL.COM",
                    EmailConfirmed = false,
                    LockoutEnabled = false,
                    SecurityStamp = "",
                    PasswordHash = passwordHasher.HashPassword(null, "123456"),
                },
                new Account()
                {
                    Id = Guid.NewGuid(),
                    UserName = "carowner1@gmail.com",
                    Email = "carowner1@gmail.com",
                    NormalizedEmail = "CAROWNER1@GMAIL.COM",
                    NormalizedUserName = "CAROWNER1@GMAIL.COM",
                    SecurityStamp = "",
                    EmailConfirmed = false,
                    LockoutEnabled = false,
                    PasswordHash = passwordHasher.HashPassword(null, "123456"),
                },
                new Account()
                {
                    Id = Guid.NewGuid(),
                    UserName = "admin1@gmail.com",
                    Email = "admin1@gmail.com",
                    NormalizedEmail = "ADMIN1@GMAIL.COM",
                    NormalizedUserName = "ADMIN1@GMAIL.COM",
                    SecurityStamp = "",
                    EmailConfirmed = false,
                    LockoutEnabled = false,
                    PasswordHash = passwordHasher.HashPassword(null, "123456"),
                },
            };

            List<IdentityUserRole<Guid>> identityRoles = new List<IdentityUserRole<Guid>>
            {
                // set role customer for user
                new IdentityUserRole<Guid>()
                {
                    UserId = accounts[0].Id,
                    RoleId = Roles[0].Id,
                },

                // set role Car Owner for user
                new IdentityUserRole<Guid>()
                {
                    UserId = accounts[1].Id,
                    RoleId = Roles[1].Id,
                },
                new IdentityUserRole<Guid>()
                {
                    UserId = accounts[2].Id,
                    RoleId = Roles[2].Id,
                },
            };

            List<Color> colors = ImportExcelFile.ImportColor("ExcelFiles/Car Rentals_Value list_Color.xlsx");
            List<BrandModel> brandModels = ImportExcelFile.ImportBrandModels("ExcelFiles/Car Rentals_Value list_Brand and model.xlsx");
            List<Address> addresses = ImportExcelFile.ImportAddresses("ExcelFiles/Addresses_Value_list.xlsx");

            modelBuilder.Entity<Account>().HasData(accounts);
            modelBuilder.Entity<IdentityRole<Guid>>().HasData(Roles);
            modelBuilder.Entity<IdentityUserRole<Guid>>().HasData(identityRoles);
            modelBuilder.Entity<Color>().HasData(colors);
            modelBuilder.Entity<BrandModel>().HasData(brandModels);
            modelBuilder.Entity<Address>().HasData(addresses);

        }
    }
}
