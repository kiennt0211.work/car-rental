﻿using CarRental.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace CarRental.Core.Data
{
    public partial class CarRentalContext : IdentityDbContext<Account,IdentityRole<Guid>,Guid>
    {
        public CarRentalContext()
        {

        }
        public CarRentalContext(DbContextOptions<CarRentalContext> options) : base(options)
        {

        }

        public virtual DbSet<Account> Accounts { get; set; } = null!;
        public virtual DbSet<Address> Addresses { get; set; } = null!;
        public virtual DbSet<Booking> Bookings { get; set; } = null!;
        public virtual DbSet<Car> Cars { get; set; } = null!;
        public virtual DbSet<Color> Colors { get; set; } = null!;
        public virtual DbSet<Feedback> Feedbacks { get; set; } = null!;
        public virtual DbSet<BrandModel> BrandModels { get; set; }
        public virtual DbSet<CarImage>? CarImages { get; set; }
        public virtual DbSet<TransactionHistory>? TransactionHistories { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
		}
		protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                        .HasOne(x => x.Address)
                        .WithMany(x => x.Accounts)
                        .HasForeignKey(x => x.AddressID);
                 

            SeedingData(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }
    }
}
